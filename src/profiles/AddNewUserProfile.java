package profiles;

/**
 * Special profile used to add new users
 */
public class AddNewUserProfile extends UserProfile {
    public AddNewUserProfile() {
        super("New User...");
    }
}
