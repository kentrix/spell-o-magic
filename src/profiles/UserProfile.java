package profiles;

/**
 * User Profile
 */
public class UserProfile{
    private final String userName;
    private double accuracy;

    private int masteredCount;
    private int faultedCount;
    private int failedCount;
    private int learnedCount;
    private long accuracyTotalCount;
    private long totalCorrectCount;
    private long score;

    public UserProfile(final String name) {
        this.userName = name;
    }

    /**
     * Reset the user profile
     */
    public void reset() {
        accuracy = 0f;
        masteredCount = 0;
        faultedCount = 0;
        failedCount = 0;
        learnedCount = 0;
        accuracyTotalCount = 0L;
        totalCorrectCount = 0L;
        score = 0;
    }

    public String getUserName() {
        return userName;
    }

    @Override
    public String toString() {
        return userName;
    }

    public void increaseMasteredCount() {
        masteredCount++;
    }

    public void increaseFaultedCount() {
        faultedCount++;
    }

    public void increaseFailedCount() {
        failedCount++;
    }

    public void increaseLearnedCount() {
        learnedCount++;
    }

    public void increaseScore(int howMuch) {
        score += howMuch;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    public int getMasteredCount() {
        return masteredCount;
    }

    public void setMasteredCount(int masteredCount) {
        this.masteredCount = masteredCount;
    }

    public int getFaultedCount() {
        return faultedCount;
    }

    public void setFaultedCount(int faultedCount) {
        this.faultedCount = faultedCount;
    }

    public int getFailedCount() {
        return failedCount;
    }

    public void setFailedCount(int failedCount) {
        this.failedCount = failedCount;
    }

    public int getLearnedCount() {
        return learnedCount;
    }

    public void setLearnedCount(int learnedCount) {
        this.learnedCount = learnedCount;
    }

    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    private void updateAccuracy() {
        accuracy = (double)totalCorrectCount/accuracyTotalCount;
    }

    /**
     * Called when the user gets a word correct
     */
    public void attemptCorrect() {
        accuracyTotalCount++;
        totalCorrectCount++;
        updateAccuracy();
    }

    /**
     * Called when the user gets a word incorrect
     */
    public void attemptIncorrect() {
        accuracyTotalCount++;
        updateAccuracy();
    }
}
