package profiles;

/**
 * Special profile which does not have a progression
 */
public class GuestUserProfile extends UserProfile {
    public GuestUserProfile() {
        super("Guest");
        reset();
    }
}
