package listeners;

import gui.UIController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ActionListener used to switch to the main menu
 */
public class SwitchToMainMenuActionListener implements ActionListener, ExtraActions{


    @Override
    public void actionPerformed(ActionEvent e) {
        extraAction();
        UIController.getInstance().switchCardView("MainMenuCard");
    }

    public void extraAction() {
    }

}
