package listeners;

import gui.UIController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ActionListener used to switch to the Media player panel
 */
public class SwitchToMediaPlayerActionListener implements ActionListener, ExtraActions {


    @Override
    public void actionPerformed(ActionEvent e) {
        extraAction();
        UIController.getInstance().switchCardView("PlayerCard");
        UIController.getInstance().fixMediaPlayerDisplay();
    }

    public void extraAction() {
    }
}
