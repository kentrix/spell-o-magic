package listeners;

import gui.UIController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Shows a dialog asking for users confirmation before switching to main menu
 */
public class SwitchToMainMenuWithConfirmationActionListener implements ExtraActions, ActionListener {
    @Override
    public void extraAction() {
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        extraAction();

        int ret = JOptionPane.showConfirmDialog(null, "<html>Do you want to return to the main menu? <br>" +
                "Your progression will be updated.</html>");
        if(ret == JOptionPane.YES_OPTION) {
            UIController.getInstance().switchCardView("MainMenuCard");
        }

    }
}
