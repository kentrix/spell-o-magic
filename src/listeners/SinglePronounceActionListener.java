package listeners;

import processHandler.workers.FestivalWorker;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ActionListener used to repeat a word
 */
public class SinglePronounceActionListener implements ActionListener{

    private final String vocal;
    private final JButton button;

    public SinglePronounceActionListener(String vocal, JButton button) {
        this.vocal = vocal;
        this.button = button;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        FestivalWorker festivalWorker = new FestivalWorker(vocal, null, button, "Repeat");
        festivalWorker.execute();
    }
}
