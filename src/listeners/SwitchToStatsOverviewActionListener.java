package listeners;

import gui.UIController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ActionListener used to switch to the Statistics overview panel
 */
public class SwitchToStatsOverviewActionListener implements ActionListener, ExtraActions {
    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        extraAction();
        UIController.getInstance().updateStatsOverviewUI();
        UIController.getInstance().switchCardView("StatsOverviewCard");
    }

    @Override
    public void extraAction() {

    }
}
