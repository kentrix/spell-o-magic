package listeners;

import com.google.gson.reflect.TypeToken;
import profiles.UserProfile;
import util.VUtil;
import util.io.DictionaryIO;
import util.io.GenericIO;
import util.io.StatsWriter;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * ActionListener when quit button is pressed
 */
public class QuitActionListener implements ActionListener, ExtraActions{

    public void extraAction() {

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        extraAction();
        StatsWriter statsWriter = new StatsWriter(System.getProperty("user.dir")
                + System.getProperty("file.separator") +
                ".voxspell" + System.getProperty("file.separator")
                + VUtil.getInstance().getUserProfile()
                                                , VUtil.getInstance().getManagedWordList());
        DictionaryIO DictionaryIO = new DictionaryIO(System.getProperty("user.dir"));
        DictionaryIO.writeToDisk();
        VUtil.getInstance().updateStoredUserProfiles();
        TypeToken<ArrayList<UserProfile>> typeToken = new TypeToken<ArrayList<UserProfile>>() {};
        GenericIO<ArrayList<UserProfile>> genericIO = new GenericIO<>(
                System.getProperty("user.dir")
                        + System.getProperty("file.separator")
                        + ".voxspell"
                        + System.getProperty("file.separator")
                        + ".userprofiles",
                (ArrayList<UserProfile>) VUtil.getInstance().getStoredUserProfiles(), typeToken);
        genericIO.writeToDisk();
        statsWriter.execute();
    }
}
