package listeners;

import gui.UIController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ActionListener used to swtich to the config panel
 */
public class SwitchConfigMenuActionListener implements ActionListener, ExtraActions{
    @Override
    public void actionPerformed(ActionEvent e) {
        UIController.getInstance().updateConfigMenuUI();
        UIController.getInstance().switchCardView("ConfigMenuCard");
    }

    @Override
    public void extraAction() {

    }
}
