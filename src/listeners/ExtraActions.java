package listeners;

/**
 * Interface implemented by most actionlistner in this project
 */
interface ExtraActions {
    void extraAction();
}
