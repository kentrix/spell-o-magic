package listeners;

import gui.UIController;
import util.Config;
import util.VUtil;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ActionListener used to start a new review
 */
public class SwitchToReviewModeActionListener implements ActionListener, ExtraActions{

    public void extraAction() {

    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        extraAction();
        if(VUtil.getInstance().getManagedWordList().getFailed().keySet().size() > 0) {
            UIController.getInstance().updateQuizWithLevel(0, Config.mode.REVIEW);
            UIController.getInstance().switchCardView("QuizCard");
        }
        else {
            JOptionPane.showMessageDialog(null, "There is nothing to review.");
        }

    }
}
