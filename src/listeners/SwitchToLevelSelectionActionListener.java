package listeners;

import gui.UIController;
import util.Config;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ActionListener used to switch to the level select menu
 */
public class SwitchToLevelSelectionActionListener implements ActionListener, ExtraActions {
    @Override
    public void actionPerformed(ActionEvent e) {
        extraAction();
        UIController.getInstance().updateLevelSelectMenuUI(Config.mode.NEW_QUIZ);
        UIController.getInstance().switchCardView("LevelSelectCard");
    }

    public void extraAction() {
    }
}
