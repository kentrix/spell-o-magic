package listeners;

import gui.UIController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * ActionListener used to start a new quiz
 */
public class SwitchToQuizModeActionListener implements ActionListener, ExtraActions{

    @Override
    public void actionPerformed(ActionEvent e) {
        extraAction();
        UIController.getInstance().switchCardView("QuizCard");
    }

    public void extraAction() {

    }
}
