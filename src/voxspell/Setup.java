package voxspell;

import com.google.gson.reflect.TypeToken;
import exceptions.InvalidFileFormatException;
import gui.UIController;
import gui.UserProfileSelector;
import profiles.AddNewUserProfile;
import profiles.GuestUserProfile;
import profiles.UserProfile;
import util.Config;
import util.VUtil;
import util.io.DictionaryIO;
import util.io.GenericIO;
import util.io.StatsReader;
import util.wordsManagement.ManagedWordList;
import util.wordsManagement.Wordlist;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;

/**
 * Set up the initial user interface and start up the main application
 */
class Setup implements Observer{

    private Wordlist wordlist = null;
    private ManagedWordList managedWordList = null;
    private UserProfile userProfile = null;
    private Map<String, Map<String, String>> dictionaryMap;
    private List<UserProfile> storedProfiles;

    private JFrame profileFrame;


    @Override
    public void update(Observable o, Object arg) {
        if(arg != null) {
            if(arg instanceof UserProfile) {
                this.userProfile = (UserProfile) arg;
            }
        }

        if(userProfile != null && !userProfile.getUserName().equalsIgnoreCase("New User...")) {
            initManagedWordList();
        }
        if(wordlist != null && managedWordList != null
                && userProfile != null && dictionaryMap != null) {
            profileFrame.dispose();
            UIController.setUIFont(new FontUIResource(Config.getInstance().getFont(),
                    Font.PLAIN, Config.getInstance().getFontSize()));
            VUtil.init(wordlist, managedWordList, storedProfiles, userProfile, dictionaryMap);
            VUtil.getInstance().processProfileRemoval();
            java.awt.EventQueue.invokeLater(UIController.getInstance());
            VUtil.getInstance().getWordList().shuffleAll();
        }

    }

    /**
     * Read the progression for a user
     */
    private void initManagedWordList() {
        String pwd = System.getProperty("user.dir");
        File dir = new File(pwd + System.getProperty("file.separator") + ".voxspell"
                            + System.getProperty("file.separator") + userProfile.getUserName());
        if(!dir.exists()) {
            dir.mkdir();
        }
        else if(!dir.isDirectory()) {
            dir.delete();
            dir.mkdir();
        }
        StatsReader reader = new StatsReader(dir.getPath());
        managedWordList = reader.readFromDisk();
    }

    /**
     * Read the word list and warn the user if the default word list is not found
     */
    private void initWordlist() {
        String pwd = System.getProperty("user.dir");
        try {
            wordlist = new Wordlist(new File(pwd + System.getProperty("file.separator") + Config.DEFAULT_WORD_LIST));
        } catch (InvalidFileFormatException e) {
            e.printStackTrace();
        }
        catch (IOException ex) {
            ex.printStackTrace();

            //Default word file is not found
            JFileChooser fileChooser = new JFileChooser();
            // Restrict the files to files only and .txt only
            fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            fileChooser.setAcceptAllFileFilterUsed(true);
            fileChooser.addChoosableFileFilter(new FileFilter() {
                @Override
                public boolean accept(File file) {
                    String fname = file.getName();
                    int index = fname.lastIndexOf('.');
                    if (index > 0) {
                        if (fname.substring(index + 1).equals("txt")) {
                            return true;
                        }
                    }
                    return false;
                }

                @Override
                public String getDescription() {
                    return ".txt";
                }
            });

            Object[] options = { "OK", "Exit" };
            int wlNotFoundret = JOptionPane.showOptionDialog(null, "Default Wordlist not found, please specifiy a valid file.", "Error",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE, null, options, options[0]);
            if(wlNotFoundret != JOptionPane.OK_OPTION) {
                System.exit(1);
            }

            for(;;) {
                int ret = fileChooser.showOpenDialog(null);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    if (VUtil.checkWordList(file)) {
                        JOptionPane.showMessageDialog(null, file.getName() + " is not a valid wordlist file.");
                    } else {
                        try {
                            wordlist = new Wordlist(file);
                            break;
                        } catch (InvalidFileFormatException | IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if (ret == JFileChooser.CANCEL_OPTION) {
                    System.exit(1);
                }
            }
        }

    }

    /**
     * Read the dictionary file if available
     */
    private void initDictionary() {
        String pwd = System.getProperty("user.dir");
        DictionaryIO dictionaryIO = new DictionaryIO(pwd);
        this.dictionaryMap = dictionaryIO.readFromDisk();
    }

    /**
     * Show the user profile selector and wait for user inputs
     */
    private void initUserProfile() {
        String pwd = System.getProperty("user.dir");
        GenericIO<ArrayList<UserProfile>> userProfileListIO = new GenericIO<>(pwd + System.getProperty("file.separator") + ".voxspell" +
                System.getProperty("file.separator") + ".userprofiles", null, new TypeToken<ArrayList<UserProfile>>() {
        });
        ArrayList<UserProfile> userProfiles = new ArrayList<>();
        userProfiles.add(new GuestUserProfile());
        storedProfiles = userProfileListIO.readFromDisk();
        if (storedProfiles != null) {
            userProfiles.addAll(userProfileListIO.readFromDisk());
        }
        else {
            storedProfiles = new ArrayList<>();
        }

        userProfiles.add(new AddNewUserProfile());

        UserProfileSelector userProfileSelector = new UserProfileSelector(userProfiles);
        userProfileSelector.updateUI();
        userProfileSelector.addObserver(this);

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        JFrame profileFrame = new JFrame();

        profileFrame.setContentPane(userProfileSelector.getContentPane());
        profileFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        profileFrame.setPreferredSize(new Dimension(500, 350));
        profileFrame.setMinimumSize(new Dimension(500, 350));
        profileFrame.setTitle("Profile Selection");
        profileFrame.pack();
        profileFrame.setVisible(true);
        profileFrame.setLocation(dimension.width/2-profileFrame.getSize().width/2,
                dimension.height/2-profileFrame.getSize().height/2);
        this.profileFrame = profileFrame;
    }

    public void start() {
        Config.initConfig(12, "Arial");
        String pwd = System.getProperty("user.dir");
        File dir = new File(pwd + System.getProperty("file.separator") + ".voxspell");
        if(!dir.exists()) {
            dir.mkdir();
        }
        else if(!dir.isDirectory()) {
            dir.delete();
            dir.mkdir();
        }
        initWordlist();
        initDictionary();
        initUserProfile();
    }
}
