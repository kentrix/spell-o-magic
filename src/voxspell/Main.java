package voxspell;

import com.alee.laf.WebLookAndFeel;


/**
 * Created by edward on 9/3/16.
 */
public class Main {
    public static void main(String[] args) {
        WebLookAndFeel.install();
        //init the necessary stuff
        Setup setup = new Setup();
        setup.start();

    }


}
