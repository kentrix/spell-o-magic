package util.wordsManagement;

import exceptions.InvalidFileFormatException;
import util.Config;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Manages the word list which the application draws its quizzes from
 */
public class Wordlist implements Iterable<List<String>> {

    private final Map<Integer, List<String>> levelList;

    public Wordlist(File file) throws InvalidFileFormatException, IOException {
        levelList = new HashMap<>();

        BufferedReader reader;
        reader = new BufferedReader(new FileReader(file));
        String curLine;
        List<String> curList;
        int level = 0;
        while (true) {
            try {
                if ((curLine = reader.readLine()) != null) {
                    if (curLine.startsWith("%")) {
                        try {
                            level = Integer.parseInt(curLine.split(" ")[1]);
                        }
                        catch(NumberFormatException e) {
                            throw new InvalidFileFormatException();
                        }
                        curList = new ArrayList<>();
                        levelList.put(level, curList);
                    } else {
                        levelList.get(level).add(curLine);
                    }
                } else {
                    break;
                }
            } catch (IOException e) {
                e.printStackTrace();
                System.err.println("Err: IO error.");
                System.exit(1);
            }
        }


    }

    /**
     * Read the wordlist and convert it to a ArrayList of Strings.
     *
     * @param path The path to the file without the last separator
     */
    public Wordlist(String path) throws InvalidFileFormatException, IOException {
        this(new File(path + System.getProperty("file.separator") + Config.getInstance().getCurrentWordList()) );

    }

    @Override
    public Iterator<List<String>> iterator() {
        return levelList.values().iterator();
    }

    public int getLevelCount() {
        return levelList.size();
    }

    /**
     * Shuffle all the lists (by level)
     */
    public void shuffleAll() {
        levelList.values().forEach(Collections::shuffle);
    }

    /**
     * Sort all the lists (by level) in alphabetical order
     */
    public void sortAll() {
        levelList.values().forEach(Collections::sort);
    }

    /**
     * Get the word list at a specified level
     * @param level
     * @return the list of strings containing all the words
     */
    public List<String> getListAtLevel(int level) {
        return levelList.get(level);
    }

    public List<String> getAllWords() {
        List<String> tmp = new ArrayList<>();
        levelList.values().forEach(tmp::addAll);
        return tmp;
    }

}
