package util.wordsManagement;

import util.VUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by edward on 8/17/16.
 */
public class ManagedWordList {

    @SafeVarargs
    public ManagedWordList(HashMap<String, Integer>... maps) {
        if(maps.length != MANAGED_FILE_NAMES.length) {
            throw new IllegalArgumentException();
        }

        this.mastered = maps[0];
        this.faulted = maps[1];
        this.failed = maps[2];
        this.learned = maps[3];

    }

    public enum wordStatus {MASTERED, FAULTED, FAILED}


    private HashMap<String, Integer> mastered;
    private HashMap<String, Integer> faulted;
    private HashMap<String, Integer> failed;
    private HashMap<String, Integer> learned;
    public static final String[] MANAGED_FILE_NAMES = {".mastered", ".faulted", ".failed", ".learned"};

    public HashMap<String, Integer> getMastered() {
        return mastered;
    }

    public HashMap<String, Integer> getFaulted() {
        return faulted;
    }

    public HashMap<String, Integer> getFailed() {
        return failed;
    }

    public HashMap<String, Integer> getLearned() {return learned; }

    /**
     * Reset all the lists, remove all elements
     */
    public void reset() {
        mastered.entrySet().removeIf(e-> true );
        faulted.entrySet().removeIf(e-> true );
        failed.entrySet().removeIf(e-> true );
        learned.entrySet().removeIf(e-> true );
    }

    private void addToAList(String word, Map<String, Integer> l) {
        word = word.toLowerCase();
        if (!l.containsKey(word)) {
            l.put(word, 1);
        } else {
            l.replace(word, (l.get(word) + 1));
        }
    }

    /**
     * Add the word to the Mastered list
     * @param word
     */
    public void addToMastered(String word) {
        VUtil.getInstance().getUserProfile().increaseMasteredCount();
        addToAList(word, mastered);
    }

    /**
     * Add the word to the Faulted list
     * @param word
     */
    public void addToFaulted(String word) {
        VUtil.getInstance().getUserProfile().increaseFaultedCount();
        addToAList(word, faulted);
    }

    /**
     * Add the word to the Failed list
     * @param word
     */
    public void addToFailed(String word) {
        VUtil.getInstance().getUserProfile().increaseFailedCount();
        addToAList(word, failed);
    }

    public void addToLearned(String word) {
        VUtil.getInstance().getUserProfile().increaseLearnedCount();
        addToAList(word, learned);
    }

    /**
     *
     */
    public ManagedWordList() {
        mastered = new HashMap<>();
        faulted = new HashMap<>();
        failed = new HashMap<>();
        learned = new HashMap<>();
    }

    public ManagedWordList(HashMap<String,Integer> mastered,
                           HashMap<String,Integer> faulted,
                           HashMap<String,Integer> failed,
                           HashMap<String,Integer> learned) {
        this.mastered = mastered;
        this.faulted = faulted;
        this.failed = failed;
        this.learned = learned;

    }


}
