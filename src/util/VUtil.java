package util;

import com.sun.istack.internal.Nullable;
import jdk.nashorn.internal.objects.annotations.Setter;
import profiles.UserProfile;
import util.wordsManagement.ManagedWordList;
import util.wordsManagement.Wordlist;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.*;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.stream.Collectors;

/**
 * A singleton class which contains various utilities used by VOXSPELL
 * Manages the words lists
 */
public class VUtil {

    private static VUtil instance;
    private Wordlist wordlist;
    private final ManagedWordList managedWordList;
    private List<Integer> levelSeq;
    private List<String> levelSeqString;
    private final List<String> fontList;
    private final Map<String, Map<String, String>> dictionaryMap;
    private final UserProfile currentUserProfile;
    private final List<UserProfile> storedUserProfiles;
    private final Lock festivalLock;


    private VUtil(Wordlist wordlist, ManagedWordList managedWordList, List<UserProfile> storedUserProfiles,
                             UserProfile userProfile, Map<String, Map<String, String>> dictionaryMap) {
        this.wordlist = wordlist;
        this.managedWordList = managedWordList;
        this.currentUserProfile = userProfile;
        this.dictionaryMap = dictionaryMap;
        this.storedUserProfiles = storedUserProfiles;
        this.festivalLock = new FestivalLock();

        levelSeq = new ArrayList<>();
        levelSeqString = new ArrayList<>();
        for (int i = 0; i < wordlist.getLevelCount(); i++) {
            levelSeqString.add("Level " + (i + 1));
            levelSeq.add(i + 1);
        }

        List<Font> fl = Arrays.asList(GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts());
        fontList = new ArrayList<>();
        fontList.addAll(fl.stream().map(Font::getFontName).collect(Collectors.toList()));

    }

    public static VUtil getInstance() {
        if (instance == null)
            throw new RuntimeException("VUtil not initiated.");

        return instance;
    }

    /**
     * Initiate the singleton class
     *
     * @param wordlist           the wordlist to be used
     * @param managedWordList    the internal word list
     * @param storedUserProfiles stored user profiles
     * @param userProfile        the user profile to be used
     * @param dictionaryMap      the dictionary, is present
     */
    public static void init(Wordlist wordlist, ManagedWordList managedWordList,List<UserProfile> storedUserProfiles,
                             UserProfile userProfile, Map<String, Map<String, String>> dictionaryMap) {
        instance = new VUtil(wordlist, managedWordList, storedUserProfiles, userProfile, dictionaryMap);
    }

    /**
     * Return a sequence {1..$MAXLEVEL}
     *
     * @return a list of integer containing the sequence
     */
    public final List<Integer> getLevelSeq() {
        return levelSeq;
    }

    /**
     * Return the Wordlist object
     * @return wordlist to be used in quizes
     */
    public final Wordlist getWordList() {
        return wordlist;
    }

    /**
     * Return the ManagedWordList object
     * @return managedWordList
     */
    public final ManagedWordList getManagedWordList() {
        return managedWordList;
    }

    /**
     * Return the full level sequence as a List of Strings
     * ie. {Level 1, Level 2...}
     * @return the list of string containing the sequence
     */
    public final List<String> getLevelSeqString() {
        return levelSeqString;
    }

    /**
     * Get the available font as a list of strings
     * @return the list of string containing the wordlist
     */
    public List<String> getFontList() {
        return fontList;
    }

    /**
     * Helper method to remove all ActionListeners from a button
     * @param button the button to remove all Action Listener from
     */
    public static void removeAllListenersFrom(JButton button) {
        while(button.getActionListeners().length != 0) {
            button.removeActionListener(button.getActionListeners()[0]);
        }
    }


    /**
     * Update the chosen worklist to be used
     * @param file the file to be used
     * @return whether the operation is successful
     */
    @Setter
    public boolean changeWordList(File file) {
        try {
            wordlist = new Wordlist(file);
            levelSeq = new ArrayList<>();
            levelSeqString = new ArrayList<>();
            for (int i = 0; i < wordlist.getLevelCount(); i++) {
                levelSeqString.add("Level " + (i + 1));
                levelSeq.add(i + 1);
            }
            return true;
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * Check if the given file is a valid wordlist file (format-wise)
     * @param file the file to be checked
     * @return where it is a valid file
     */
    public static boolean checkWordList(File file) {
        try {
            new Wordlist(file);
        }
        catch (Exception e) {
            return true;
        }

        return false;

    }

    /**
     * Add a Map to the current dictionary
     * @param infoMap The map to be added
     */
    public void jointDictionaryMap(Map<String, Map<String, String>>infoMap) {
        this.dictionaryMap.putAll(infoMap);
    }

    public Map<String, Map<String, String>> getDictionaryMap() {
        return dictionaryMap;
    }

    /**
     * Helper method to strip all punctuations from a string
     * @param str
     * @return
     */
    public static String stripPunctuation(String str) {
        return str.replaceAll("[^a-zA-Z ]", "");
    }

    /**
     * Get the current user profile
     * @return the current user profile
     */
    public UserProfile getUserProfile() {
        return currentUserProfile;
    }

    /**
     * Update the internal list of user profiles
     */
    public void updateStoredUserProfiles() {
        boolean found = false;
        int i = 0;
        for (; i < storedUserProfiles.size(); i++) {
            if (storedUserProfiles.get(i).getUserName().equalsIgnoreCase(currentUserProfile.getUserName())) {
                found = true;
                break;
            }
        }

        if(found && !currentUserProfile.getUserName().equalsIgnoreCase("Guest")) {
            storedUserProfiles.set(i, currentUserProfile);
        }
        else if(!currentUserProfile.getUserName().equalsIgnoreCase("Guest")){
            //!found
            storedUserProfiles.add(currentUserProfile);
        }
    }

    /**
     * Get the internal list of user profiles
     * @return
     */
    public List<UserProfile> getStoredUserProfiles() {
        return storedUserProfiles;
    }

    public void addUserProfile(UserProfile userProfile) {
        if(!storedUserProfiles.contains(userProfile)) {
            storedUserProfiles.add(userProfile);
        }
    }

    /**
     * Remove the user profiles from the internal list
     */
    public void processProfileRemoval() {
        List<UserProfile> profileList = Config.getInstance().getQueuedProfilesForRemoval();
        for (UserProfile profile:
             profileList) {
            Iterator<UserProfile> iter = storedUserProfiles.iterator();

            while(iter.hasNext()) {

                UserProfile storedProfile = iter.next();

                if(profile.getUserName().equalsIgnoreCase(storedProfile.getUserName())) {
                    iter.remove();
                    File file = new File(
                            System.getProperty("user.dir")
                                    + System.getProperty("file.separator")
                                    + ".voxspell"
                                    + System.getProperty("file.separator")
                                    + storedProfile.getUserName());
                    if (file.exists() && file.isDirectory()) {
                        file.delete();
                    }
                }
            }
        }
    }

    /**
     * Helper method to get last appearing number in a String
     * @param text the text to process
     * @return the number representation
     */
    @Nullable
    public static long getLastNumFromString(String text) {
        String ar[] = text.split(" ");
        return Long.parseLong(ar[ar.length - 1]);
    }

    /**
     * Get the festival lock object
     * @return
     */
    public Lock getLockObject() {
        return festivalLock;
    }
}
