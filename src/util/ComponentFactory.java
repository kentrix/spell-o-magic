package util;

import gui.About;
import gui.QuitDialog;
import gui.UIController;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Map;

/**
 * Factory to produce some JComponents
 */
public class ComponentFactory {


    /**
     * Get a JComboBox component which allows change to currently used voices
     * @return A JComboBox instance
     */
    public static JComboBox newVoiceSelectionComboBox() {

        JComboBox voiceComboBox;

        voiceComboBox = new JComboBox();
        voiceComboBox.setModel(new DefaultComboBoxModel(Config.getInstance().voiceMap.values().toArray()));
        voiceComboBox.getModel().setSelectedItem(Config.getInstance().voiceMap.get(Config.getInstance().getVoice()));

        voiceComboBox.addActionListener(actionEvent -> {

            for (Map.Entry<String, String> e : Config.getInstance().voiceMap.entrySet()) {
                if (e.getValue().equals(voiceComboBox.getModel().getSelectedItem())) {
                    Config.getInstance().setVoice(e.getKey());
                    break;
                }
            }
        });

        return voiceComboBox;
    }

    /**
     * Create and show a JFrame to ask the user what action should be performed
     */
    public static void createAndShowQuitDialog() {
        JFrame exitFrame = new JFrame();
        exitFrame.setTitle("Confirm Quit");
        UIController.getInstance().disableRootFrame();
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();

        exitFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        exitFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                UIController.getInstance().enableRootFrame();
            }
        });
        QuitDialog quitDialog = new QuitDialog();
        exitFrame.setContentPane(quitDialog.$$$getRootComponent$$$());

        exitFrame.pack();
        exitFrame.setVisible(true);

        exitFrame.setLocation(dimension.width/2-exitFrame.getSize().width/2,
                dimension.height/2-exitFrame.getSize().height/2);
    }


    /**
     * Create and show the about dialog
     */
    public static void createAndShowAboutDialog() {
        JFrame aboutFrame = new JFrame();
        aboutFrame.setTitle("About");
        UIController.getInstance().disableRootFrame();
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();

        aboutFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        aboutFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                UIController.getInstance().enableRootFrame();
            }
        });
        About about = new About(aboutFrame);
        aboutFrame.setContentPane(about.$$$getRootComponent$$$());

        aboutFrame.pack();
        aboutFrame.setVisible(true);

        aboutFrame.setLocation(dimension.width/2-aboutFrame.getSize().width/2,
                dimension.height/2-aboutFrame.getSize().height/2);

    }
}
