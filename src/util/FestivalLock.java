package util;


import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * Festival Lock class intended to provide safety for festival processes
 */
class FestivalLock implements Lock {

    private boolean isLocked;

    public FestivalLock() {
        isLocked = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void lock() {
        this.isLocked = true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Deprecated
    public void lockInterruptibly() throws InterruptedException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean tryLock() {
        if(isLocked) {
            return false;
        }
        else {
            isLocked = true;
            return true;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Deprecated
    public boolean tryLock(long l, TimeUnit timeUnit) throws InterruptedException {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unlock() {
        isLocked = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Deprecated
    public Condition newCondition() {
        return null;
    }
}
