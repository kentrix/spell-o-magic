package util.io;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import util.wordsManagement.ManagedWordList;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Read the stats from disk as JSON
 */
public class StatsReader implements Readable{

    private final String path;

    public StatsReader(final String path) {
        this.path = path;
    }

    public ManagedWordList readFromDisk() {
        List<HashMap<String, Integer>> manList = new ArrayList<>();
        HashMap<String, Integer> mastered = new HashMap<>();
        HashMap<String, Integer> failed = new HashMap<>();
        HashMap<String, Integer> faulted = new HashMap<>();
        HashMap<String, Integer> learned = new HashMap<>();
        manList.add(mastered);
        manList.add(faulted);
        manList.add(failed);
        manList.add(learned);

        for (int i = 0; i < ManagedWordList.MANAGED_FILE_NAMES.length; i++) {
            File file = new File(path + System.getProperty("file.separator") + ManagedWordList.MANAGED_FILE_NAMES[i]);
            if (file.exists()) {
                BufferedReader reader = null;
                try {
                    reader = new BufferedReader(new FileReader(file));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    System.exit(1);
                }

                StringBuilder sb = new StringBuilder();
                String line;
                try {
                    while ((line = reader.readLine()) != null) {
                        sb.append(line);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String json = sb.toString();
                Gson gson = new Gson();

                TypeToken<HashMap<String, Integer>> tk = new TypeToken<HashMap<String, Integer>>() {};
                manList.set(i, gson.fromJson(json, tk.getType()));
            }
        }

        return new ManagedWordList(manList.get(0), manList.get(1), manList.get(2), manList.get(3));

    }
}
