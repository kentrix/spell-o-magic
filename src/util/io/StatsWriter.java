package util.io;

import com.google.gson.Gson;
import util.wordsManagement.ManagedWordList;

import javax.swing.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Responsible for writing the statistics to the disk.
 * @author edward
 * @see SwingWorker
 */
public class StatsWriter extends SwingWorker<Boolean, Void> {

    private final ManagedWordList managedWordList;
    private final String pathStr;

    /**
     * Construct a StatsWrite object with path and a ManagedWordList
     * @param path path to the directory where files are to be saved
     * @param managedWordList a ManagedWordList object
     */
    public StatsWriter(String path, ManagedWordList managedWordList) {
        pathStr = path;
        this.managedWordList = managedWordList;
    }

    /**
     * A private method that does the writing to the disk
     */
    private void writeToDisk() {
        List<HashMap<String, Integer>> manList = new ArrayList<>();
        manList.add(managedWordList.getMastered());
        manList.add(managedWordList.getFaulted());
        manList.add(managedWordList.getFailed());
        manList.add(managedWordList.getLearned());
        for (int i = 0; i < ManagedWordList.MANAGED_FILE_NAMES.length; i++) {
            File file = new File(pathStr + System.getProperty("file.separator") + ManagedWordList.MANAGED_FILE_NAMES[i]);
            if (file.exists()) {
                file.delete();
                try {
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    file.createNewFile();
                    file.setWritable(true);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Gson gson = new Gson();
            String json = gson.toJson(manList.get(i));
            try {
                BufferedWriter br = new BufferedWriter(new FileWriter(file));
                br.write(json);
                br.flush();
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Boolean doInBackground() throws Exception {
        writeToDisk();
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void done() {
        System.exit(0);
    }
}
