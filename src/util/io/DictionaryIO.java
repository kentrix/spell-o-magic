package util.io;

import com.google.gson.Gson;
import com.sun.istack.internal.NotNull;
import util.VUtil;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Dictionary Input/Output helper class
 */
public class DictionaryIO implements Readable, Writable{

    private static final String DICTIONARY_FILE_NAME = ".dictionary";
    private final File file;

    public DictionaryIO(String path) {
        file = new File(path + System.getProperty("file.separator") + DICTIONARY_FILE_NAME);
    }

    @Override
    public void writeToDisk() {
        if (file.exists()) {
            file.delete();

            try{
                file.createNewFile();
            }
            catch (IOException ioex) {
                ioex.printStackTrace();
                System.exit(1);
            }

        }
        else {
            try{
                file.createNewFile();
                file.setWritable(true);
            }
            catch (IOException ioex) {
                ioex.printStackTrace();
                System.exit(1);
            }
        }

        Gson gson = new Gson();
        String json = gson.toJson(VUtil.getInstance().getDictionaryMap());

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(json);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    @Override
    @NotNull
    public Map<String, Map<String, String>> readFromDisk() {
        if(file.exists()) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(file));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.exit(1);
            }

            StringBuilder sb = new StringBuilder();
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            String json = sb.toString();
            Gson gson = new Gson();
            Map<String, Map<String, String>> map = gson.fromJson(json, Map.class);
            return map;
        }
        else {
            return new HashMap<>();
        }

    }
}
