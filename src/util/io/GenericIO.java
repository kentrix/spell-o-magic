package util.io;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.istack.internal.Nullable;

import java.io.*;

/**
 * Generic data reader/write which utilizes JSON for serialization and deserialization
 */
public class GenericIO<T> implements Readable<T>, Writable{
    private final TypeToken typeToken;
    private final File file;
    private final T data;

    /**
     * Construct a GenericIO object
     * @param path path to the file
     * @param data date to be written
     * @param typeToken a TypeToken of the class being written
     */
    public GenericIO(String path, T data, TypeToken typeToken) {
        file = new File(path);
        this.data = data;
        this.typeToken = typeToken;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    @Nullable
    public T readFromDisk() {
        if(file.exists()) {
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new FileReader(file));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                System.exit(1);
            }

            StringBuilder sb = new StringBuilder();
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            String json = sb.toString();
            Gson gson = new Gson();

            T readData;
            if(typeToken != null) {
                readData = gson.fromJson(json, typeToken.getType());
            }
            else {
                TypeToken<T> tk = new TypeToken<T>() {};
                readData = gson.fromJson(json, tk.getType());
            }
            return readData;

        }
        else {
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void writeToDisk() {
        if (data == null) {
            return;
        }

        if (file.exists()) {
            file.delete();

            try{
                file.createNewFile();
            }
            catch (IOException ioex) {
                ioex.printStackTrace();
                System.exit(1);
            }

        }
        else {
            try{
                file.createNewFile();
                file.setWritable(true);
            }
            catch (IOException ioex) {
                ioex.printStackTrace();
                System.exit(1);
            }
        }

        Gson gson = new Gson();
        String json = gson.toJson(data);

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            writer.write(json);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
