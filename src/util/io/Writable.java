package util.io;

/**
 * Classes where data can be written to disk should implement this interface
 */
interface Writable {
    /**
     * Write the data to disk
     */
    void writeToDisk();
}
