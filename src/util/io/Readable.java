package util.io;

import com.sun.istack.internal.Nullable;


/**
 * Classes where data can be obtained from disk should implement this interface
 */
interface Readable<T> {
    /**
     * Read the data from disk
     *
     * @return the date read
     */
    @Nullable
    T readFromDisk();
}
