package util;

import gui.UIController;
import processHandler.processes.FestivalGetVoicesListProcess;
import profiles.UserProfile;
import voxspell.Main;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * Singleton class that stores all the settings
 */
public class Config {
    public static final int INITIAL_VOLUME = 50;
    private static Config config = null;
    private int fontSize;
    private String voice;
    private String font;
    public static final int MAX_FONT_SIZE = 20;
    public static final int MIN_FONT_SIZE = 8;
    public static String[] VOICES_LIST;
    public static final int TEST_PER_LEVEL = 10;
    public final ImageIcon PLAY_ICON = new ImageIcon(
                    getClass().getResource("/resources/icons/Media-Controls-Play-icon.png"));
    public final ImageIcon PAUSE_ICON = new ImageIcon(
                    getClass().getResource("/resources/icons/Media-Controls-Pause-icon.png"));
    public final ImageIcon VOLUME_UP_ICON = new ImageIcon(
                    getClass().getResource("/resources/icons/Media-Controls-Volume-Up-icon.png"));
    public final ImageIcon MUTE_ICON = new ImageIcon(
                    getClass().getResource("/resources/icons/Media-Controls-Mute-icon.png"));
    /*
    public final String ALT_MEDIA_FILE = "zip://" +
            new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getAbsolutePath()
            + '!' + System.getProperty("file.separator") + "resources" + System.getProperty("file.separator")
            + "big_buck_bunny_1_minute.avi";
    public final String DEFAULT_MEDIA_FILE = "zip://" +
            new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getAbsolutePath()
            + '!' + System.getProperty("file.separator") + "resources" + System.getProperty("file.separator")
            + "edited.avi";
    */
    //public final String DEFAULT_MEDIA_FILE = new File(getClass().getResource("/resources/big_buck_bunny_1_minute.avi").getFile()).getPath();
    //public final String ALT_MEDIA_FILE = new File(getClass().getResource("/resources/edited.avi").getFile()).getPath();
    public final String DEFAULT_MEDIA_FILE = System.getProperty("user.dir") +  System.getProperty("file.separator") + "bbb.avi";
    public final String ALT_MEDIA_FILE = System.getProperty("user.dir") +  System.getProperty("file.separator") + "edited.avi";
    public final String GAME_INTRO_AUDIO = "zip://"
            + new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getAbsolutePath()
            + '!' + System.getProperty("file.separator") + "resources" + System.getProperty("file.separator") + "sound"
            + System.getProperty("file.separator") + "game-sound-intro-to-game.wav";
    public final String GAME_CORRECT_AUDIO = "zip://"
            + new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getAbsolutePath()
            + '!' + System.getProperty("file.separator") + "resources" + System.getProperty("file.separator") + "sound"
            + System.getProperty("file.separator") + "game-sound-correct.wav";
    public final String GAME_WRONG_AUDIO = "zip://"
            + new File(Main.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()).getAbsolutePath()
            + '!' + System.getProperty("file.separator") + "resources" + System.getProperty("file.separator") + "sound"
            + System.getProperty("file.separator") + "game-sound-wrong.wav";
    //public final String GAME_INTRO_AUDIO = getClass().getResource("/resources/sound/game-sound-intro-to-game.wav").getFile();
    //public final String GAME_CORRECT_AUDIO = getClass().getResource("/resources/sound/game-sound-correct.wav").getFile();
    //public final String GAME_WRONG_AUDIO = getClass().getResource("/resources/sound/game-sound-wrong.wav").getFile();
    public final Map<String, String> voiceMap;
    public final static int TTS_DELAY = 500;
    public final static String DEFAULT_WORD_LIST = "NZCER-spelling-lists.txt";
    private final File DEFAULT_WORD_FILE;
    private String currentWordList;
    private File currentWordFile;
    private final java.util.List<UserProfile> profileList = new ArrayList<>();
    public static final String EXP_SENT_API_URL = "https://api.pearson.com/v2/dictionaries/ldoce5/entries?headword=%s&apikey=B5THfewobAbZq79N8vPBrJpr9SWTBXs2";


    private Config(int fontSize, String font) throws URISyntaxException {
        this.fontSize = fontSize;
        this.font = font;
        String pwd = System.getProperty("user.dir");
        DEFAULT_WORD_FILE = new File(pwd + System.getProperty("file.separator") + DEFAULT_WORD_LIST);
        currentWordList = DEFAULT_WORD_LIST;
        currentWordFile = DEFAULT_WORD_FILE;
        voiceMap = new HashMap<>();
        // dictionary of voices
        voiceMap.put("rab_diphone", "Rab");
        voiceMap.put("akl_nz_jdt_diphone", "New Zealand Jdt");
        voiceMap.put("kal_diphone", "Kal");
        // The following voices do not seem to do anything
        // hence disabled
        /*
        voiceMap.put("cmu_us_rms_arctic", "US Rms");
        voiceMap.put("cmu_us_slt_arctic", "US Slt");
        voiceMap.put("cmu_us_bdl_arctic", "US bdl");
        voiceMap.put("cmu_us_clb_arctic", "US clb");
        */

        Map<String, String> tmpMap = new HashMap<>();

        //Clean out unavailable voices
        for(String str : VOICES_LIST) {
            if(voiceMap.containsKey(str)) {
                tmpMap.put(str, voiceMap.get(str));
            }
        }
        this.voiceMap.clear();
        this.voiceMap.putAll(tmpMap);

        this.voice = (String)(voiceMap.keySet().toArray())[0];


    }

    /**
     * Intitalise the singleton, must be called before obtaining an instance of this class
     * @param fontSize
     * @param font
     */
    public static void initConfig(int fontSize, String font) {
        try {
            VOICES_LIST = (new BufferedReader(new InputStreamReader(new FestivalGetVoicesListProcess().startAndGetProcess().getInputStream()))).readLine().split(" ");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (config == null) {
            try {
                config = new Config(fontSize, font);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            UIController.setUIFont(new FontUIResource("Arial", Font.PLAIN, fontSize));
        }


    }

    /**
     * Change the voice
     * @param voice
     */
    public void setVoice(String voice) {
        this.voice = voice;
    }

    /**
     * Change the size of the font
     * @param fontSize
     */
    public void setFontSize(int fontSize) {
        this.fontSize = fontSize;
    }

    /**
     * Get the currently used voice
     * @return
     */
    public String getVoice() {
        return voice;
    }


    /**
     * Get currently used font size
     * @return
     */
    public int getFontSize() {
        return fontSize;
    }

    /**
     * Get an instance of this singleton class
     * @return
     */
    public static Config getInstance() {
        if(config == null) {
            throw new RuntimeException("Config not initiated");
        }
        return config;
    }

    /**
     * Get the string representation of the font currently being used
     * @return
     */
    public String getFont() {
        return font;
    }

    /**
     * Set the font being used
     * @param font the string representation of the font
     */
    public void setFont(String font) {
        this.font = font;
    }

    /**
     * Set the word list to be used
     * @return
     */
    public String getCurrentWordList() {
        return currentWordList;
    }

    /**
     * Get the current word list being used
     * @param currentWordList
     */
    public void setCurrentWordList(String currentWordList) {
        this.currentWordList = currentWordList;
    }

    /**
     * Get the currently used word file
     * @return
     */
    public File getCurrentWordFile() {
        return currentWordFile;
    }

    /**
     * Set the currently used word file
     * @param currentWordFile
     */
    public void setCurrentWordFile(File currentWordFile) {
        this.currentWordFile = currentWordFile;
    }

    /**
     * A enum representing all the possible game modes
     */
    public enum mode {
        NEW_QUIZ, REVIEW, LEARNING
    }

    /**
     * Queue a user profile for removal
     *
     * @param profile user profile to be removed
     */
    public void queueProfileRemoval(UserProfile... profile) {
        profileList.addAll(Arrays.asList(profile));
    }

    /**
     * Get the queued user files for removal
     * @return a list of profiles to be removed
     */
    public java.util.List<UserProfile> getQueuedProfilesForRemoval() {
        return profileList;
    }
}
