package models;

import gui.EndOfLevel;
import gui.FestivalButtonsToggleable;
import gui.StatPanel;
import gui.UIController;
import listeners.SinglePronounceActionListener;
import processHandler.workers.FestivalWorker;
import processHandler.workers.WavWorker;
import processHandler.workers.WavWorkerWithFestival;
import util.Config;
import util.VUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Quiz model used when reviewing
 */
public class ReviewModel extends QuizModel{

    private int totalToTest;

    public ReviewModel(JFormattedTextField textField, StatPanel statPanel, JProgressBar progressBar,
                       JButton validateButton, JButton repeatButton, JButton exampleSentceButton,
                       JLabel scoreLabel, JLabel totalScoreLabel, JLabel totalAccuracyLabel,
                       FestivalButtonsToggleable festivalButtonsToggleable) {
        super(textField, statPanel, progressBar, validateButton, 0, repeatButton, exampleSentceButton,
                scoreLabel, totalScoreLabel, totalAccuracyLabel, festivalButtonsToggleable);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected void firstStartAction() {
        progressBar.setModel(new DefaultBoundedRangeModel(0, 1, 0, quizStrings.size()));
        repeatButton.setEnabled(true);
        FestivalWorker ps = new FestivalWorker("Please Spell the word: " + quizStrings.get(count), festivalButtonsToggleable);
        ps.execute();
        setChanged();
        notifyObservers(quizStrings.get(count));
        VUtil.removeAllListenersFrom(repeatButton);
        repeatButton.addActionListener(new SinglePronounceActionListener(quizStrings.get(count), repeatButton));
        validateButton.setText("Submit");

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void firstTryCorrectAction() {
        textField.setText("");
        FestivalWorker ps = new FestivalWorker("correct", count < totalToTest - 1 ?
                "Please spell the word: " + quizStrings.get(count + 1) : null, validateButton, null, festivalButtonsToggleable);
        WavWorker wavWorker = new WavWorkerWithFestival(Config.getInstance().GAME_CORRECT_AUDIO, ps);
        wavWorker.execute();
        setChanged();
        notifyObservers(count < totalToTest -1 ? quizStrings.get(count + 1) : null);
        scoreLabel.setText("Session Score: " + ((VUtil.getLastNumFromString(scoreLabel.getText()) + level * 5)));
        VUtil.getInstance().getUserProfile().increaseScore(level * 5);
        VUtil.getInstance().getManagedWordList().getFailed().remove(quizStrings.get(count));
        statPanel.correctAt(count);
        statPanel.setWordAt(count, quizStrings.get(count));
        progressBar.setValue(count + 1);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void firstTryIncorrectAction() {

        textField.setText("");
        FestivalWorker ps = new FestivalWorker("Incorrect, try once more,"
                + quizStrings.get(count) + "," + quizStrings.get(count), festivalButtonsToggleable);
        WavWorker wavWorker = new WavWorkerWithFestival(Config.getInstance().GAME_WRONG_AUDIO, ps);
        wavWorker.execute();
        statPanel.wrongAt(count);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void secondTryCorrectAction() {
        textField.setText("");

        FestivalWorker ps = new FestivalWorker("correct", count < totalToTest - 1 ?
                "Please spell the word: " + quizStrings.get(count + 1) : null, festivalButtonsToggleable);
        WavWorker wavWorker = new WavWorkerWithFestival(Config.getInstance().GAME_CORRECT_AUDIO, ps);
        wavWorker.execute();
        setChanged();
        notifyObservers(count < totalToTest -1 ? quizStrings.get(count + 1) : null);

        scoreLabel.setText("Session Score: " + ((VUtil.getLastNumFromString(scoreLabel.getText()) + level * 5)));
        VUtil.getInstance().getUserProfile().increaseScore(level * 5);
        statPanel.correctAt(count);
        statPanel.setWordAt(count, quizStrings.get(count));
        progressBar.setValue(count + 1);
        VUtil.getInstance().getManagedWordList().addToFaulted(quizStrings.get(count));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void updateRepeatButton() {

        VUtil.removeAllListenersFrom(repeatButton);
        repeatButton.addActionListener(new SinglePronounceActionListener(quizStrings.get(count), repeatButton));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void bothWrongAction() {

        textField.setText("");

        FestivalWorker ps = new FestivalWorker("Incorrect", count < totalToTest - 1 ?
                "Please spell the word:  " + quizStrings.get(count + 1) : null, festivalButtonsToggleable);
        WavWorker wavWorker = new WavWorkerWithFestival(Config.getInstance().GAME_WRONG_AUDIO, ps);
        wavWorker.execute();
        setChanged();
        notifyObservers(count < totalToTest -1 ? quizStrings.get(count + 1) : null);

        statPanel.wrongAt(count);
        statPanel.setWordAt(count, quizStrings.get(count));
        progressBar.setValue(count + 1);

        VUtil.getInstance().getManagedWordList().addToFailed(quizStrings.get(count));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void finished() {

        VUtil.removeAllListenersFrom(validateButton);
        validateButton.setEnabled(true);
        validateButton.setText("Finished");
        repeatButton.setEnabled(false);
        UIController.getInstance().disableRootFrame();
        JFrame eolFrame = new JFrame();

        eolFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                UIController.getInstance().enableRootFrame();
            }
        });
        EndOfLevel endOfLevel = new EndOfLevel(level, correctCount >= totalToTest, eolFrame, Config.mode.REVIEW);
        endOfLevel.updateUI();
        eolFrame.setContentPane(endOfLevel.$$$getRootComponent$$$());
        eolFrame.pack();
        eolFrame.setVisible(true);

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        eolFrame.setLocation(dimension.width/2-eolFrame.getSize().width/2,
                dimension.height/2-eolFrame.getSize().height/2);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isQuizFinished() {
        return (count < totalToTest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initQuizString() {
        quizStrings = new ArrayList<>(VUtil.getInstance().getManagedWordList().getFailed().keySet());
        Collections.shuffle(quizStrings);

        totalToTest = VUtil.getInstance().getManagedWordList().getFailed().keySet().size();
        totalToTest = totalToTest >= 10 ? 10 : totalToTest;
        statPanel.hideUnusedAccuracyPanes(10 - totalToTest);
        DefaultBoundedRangeModel defaultBoundedRangeModel = new DefaultBoundedRangeModel();
        defaultBoundedRangeModel.setMaximum(totalToTest);
        defaultBoundedRangeModel.setMinimum(0);
        progressBar.setModel(defaultBoundedRangeModel);

        this.addObserver(new ExampleSentenceButtonObserver(exampleSentenceButton));
    }
}
