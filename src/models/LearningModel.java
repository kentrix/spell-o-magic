package models;

import gui.FestivalButtonsToggleable;
import gui.LearningMode;
import processHandler.workers.FestivalWorker;
import processHandler.workers.WavWorker;
import processHandler.workers.WavWorkerWithFestival;
import util.Config;
import util.VUtil;

import javax.swing.*;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

/**
 * A model to be used with the Learning Mode
 */
public class LearningModel extends Observable{

    private final int level;
    private final LearningMode learningMode;
    private List<String> quizStrings;
    private int count;
    private final FestivalButtonsToggleable festivalButtonsToggleable;

    public LearningModel(int level, LearningMode learningMode) {

        this.level = level;
        this.learningMode = learningMode;
        this.festivalButtonsToggleable = learningMode;
        this.addObserver(learningMode);
        initQuizString();

        count = 0;

    }

    private void initQuizString() {
        quizStrings = VUtil.getInstance().getWordList().getListAtLevel(level);
        Collections.shuffle(quizStrings);
    }

    /**
     * Verify if the word is valid and perform action accordingly
     * @param word
     */
    public void validateWord(String word) {
        if(word.isEmpty()) {
            return;
        }
        word = VUtil.stripPunctuation(word);

        if(quizStrings.get(count).equalsIgnoreCase(word)) {
            //Correct
            VUtil.getInstance().getManagedWordList().addToLearned(word);
            learningMode.clearTextField();
            learningMode.pushWordToLearnedPane(quizStrings.get(count));
            count++;
            learningMode.updateProgress(count);
            if(count >= Config.TEST_PER_LEVEL) {
                //finished
                SwingWorker worker = new FestivalWorker("Good Work! You have just learnt ten words!");
                worker.execute();
                learningMode.finish();
            }
            else {
                SwingWorker worker = new FestivalWorker("Good work!", quizStrings.get(count), festivalButtonsToggleable);
                WavWorker wavWorker = new WavWorkerWithFestival(Config.getInstance().GAME_CORRECT_AUDIO, worker);
                wavWorker.execute();
                setChanged();
                notifyObservers(quizStrings.get(count));
            }
        }
        else {
            SwingWorker worker = new FestivalWorker("Incorrect!", quizStrings.get(count), festivalButtonsToggleable);
            WavWorker wavWorker = new WavWorkerWithFestival(Config.getInstance().GAME_WRONG_AUDIO, worker);
            wavWorker.execute();
            learningMode.clearTextField();
        }
    }

    /**
     * Start the learning mode
     */
    public void start() {
        String word = quizStrings.get(count);
        learningMode.changeActionListener();
        SwingWorker worker = new FestivalWorker(quizStrings.get(count), festivalButtonsToggleable);
        worker.execute();
        setChanged();
        notifyObservers(word.toLowerCase());

    }

}
