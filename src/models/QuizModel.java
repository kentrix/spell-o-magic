package models;

import com.sun.istack.internal.NotNull;
import gui.FestivalButtonsToggleable;
import gui.StatPanel;
import util.VUtil;

import javax.swing.*;
import java.util.List;
import java.util.Observable;

/**
 * A generic quiz model
 */
public abstract class QuizModel extends Observable{
    private final JLabel totalAccuracyLabel;
    final JFormattedTextField textField;
    final StatPanel statPanel;
    final JProgressBar progressBar;
    int count;
    final JButton validateButton;
    final JButton repeatButton;
    final JButton exampleSentenceButton;
    List<String> quizStrings;
    private boolean failedOnce;
    private boolean isStart;
    int correctCount = 0;
    final int level;
    final JLabel scoreLabel;
    private final JLabel totalScoreLabel;
    final FestivalButtonsToggleable festivalButtonsToggleable;

    QuizModel(JFormattedTextField textField, StatPanel statPanel,
              JProgressBar progressBar, JButton validateButton, int level, JButton repeatButton,
              JButton exampleSentceButton, JLabel scoreLabel, JLabel totalScoreLabel,
              JLabel totalAccuracyLabel, FestivalButtonsToggleable festivalButtonsToggleable) {
        this.level = level;
        count = 0;
        this.progressBar = progressBar;
        this.statPanel = statPanel;
        this.textField = textField;
        this.validateButton = validateButton;
        this.repeatButton = repeatButton;
        this.exampleSentenceButton = exampleSentceButton;
        this.isStart = true;
        this.scoreLabel = scoreLabel;
        this.totalAccuracyLabel = totalAccuracyLabel;
        this.totalScoreLabel = totalScoreLabel;
        this.festivalButtonsToggleable = festivalButtonsToggleable;
        failedOnce = false;
        VUtil.removeAllListenersFrom(validateButton);
        validateButton.addActionListener(e -> validateWord());
        initQuizString();
        validateButton.setText("<html><h1>Start</h1></html>");
    }


    private void validateWord() {

        if (isStart) {
            firstStartAction();
            textField.requestFocusInWindow();
            isStart = false;
            return;
        }

        if (isQuizFinished()) {
            if (!failedOnce) {
                if(isUserInputEmpty()) {
                    return;
                }

                if (verifyString()) {
                    // 1st try correct
                    VUtil.getInstance().getUserProfile().attemptCorrect();
                    firstTryCorrectAction();
                    updateUIAction();
                    count++;
                    correctCount++;
                    if (isQuizFinished()) {
                        updateRepeatButton();
                    } else {
                        finished();
                    }
                } else {
                    // 1st try incorrect
                    VUtil.getInstance().getUserProfile().attemptIncorrect();
                    failedOnce = true;
                    firstTryIncorrectAction();
                    updateUIAction();
                }

            } else {
                failedOnce = false;
                if(isUserInputEmpty()) {
                    return;
                }

                //Failed once
                if (verifyString()) {
                    //2nd try correct
                    VUtil.getInstance().getUserProfile().attemptCorrect();
                    secondTryCorrectAction();
                    updateUIAction();

                    count++;
                    correctCount++;
                    if (isQuizFinished()) {
                        updateRepeatButton();
                    } else {
                        finished();
                    }
                } else {
                    // failed twice
                    VUtil.getInstance().getUserProfile().attemptIncorrect();
                    bothWrongAction();
                    updateUIAction();
                    failedOnce = false;
                    count++;

                    if (isQuizFinished()) {
                        updateRepeatButton();
                    } else {
                        finished();
                    }
                }
            }
        } else {
            // count is large enough
            finished();
        }
    }


    /**
     * Called when the quiz is freshly started
     */
    abstract protected void firstStartAction();

    /**
     * Called when the user got the word correct on the first try
     */
    abstract protected void firstTryCorrectAction();

    /**
     * Called when the user got the word incorrecet on the first try
     */
    abstract protected void firstTryIncorrectAction();

    /**
     * Called when the user got the word correct on the second try
     */
    abstract protected void secondTryCorrectAction();

    /**
     * Called when the user got the word wrong both times
     */
    abstract protected void bothWrongAction();

    /**
     * Called when the repeat word button need to updated with a new word
     */
    abstract protected void updateRepeatButton();

    /**
     * Called when the quiz is finished
     */
    abstract protected void finished();

    /**
     * Returns whether the quiz is finished to determine next step
     */
    abstract protected boolean isQuizFinished();

    /**
     * Initialise the list of Strings to be used the quiz
     */
    abstract protected void initQuizString();

    /**
     * Helper method to update accuracy label and score label
     */
    private void updateLabels() {
        totalAccuracyLabel.setText(String.format("Overall Accuracy: %.2f%%",
                                    VUtil.getInstance().getUserProfile().getAccuracy() * 100));
        totalScoreLabel.setText("Total Score: " + VUtil.getInstance().getUserProfile().getScore());
    }

    /**
     * Helper method to verify the user input
     *
     * @return true if the user input is correct, false otherwise
     */
    @NotNull
    private boolean verifyString() {
        return VUtil.stripPunctuation(quizStrings.get(count)).equalsIgnoreCase(VUtil.stripPunctuation(textField.getText()));
    }

    /**
     * Helper method to test whether the user input is empty
     * @return true if the user input is empty, false otherwise
     */
    @NotNull
    private boolean isUserInputEmpty() {
        return textField.getText().isEmpty();
    }

    /**
     * Helper method to update the labels and force focus on the text field
     */
    private void updateUIAction() {
        updateLabels();
        textField.requestFocusInWindow();
    }
}
