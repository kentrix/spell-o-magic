package models;

import gui.EndOfLevel;
import gui.FestivalButtonsToggleable;
import gui.StatPanel;
import listeners.SinglePronounceActionListener;
import processHandler.workers.FestivalWorker;
import processHandler.workers.WavWorker;
import processHandler.workers.WavWorkerWithFestival;
import util.Config;
import util.VUtil;

import javax.swing.*;
import java.awt.*;
import java.util.Collections;

/**
 * Quiz model used when starting at a level
 */
public class NewQuizModel extends QuizModel {


    public NewQuizModel(JFormattedTextField textField, StatPanel statPanel, JProgressBar progressBar,
                        JButton validateButton, int level, JButton repeatButton, JButton exampleSentenceButton,
                        JLabel scoreLabel, JLabel totalScoreLabel, JLabel totalAccuracyLabel,
                        FestivalButtonsToggleable festivalButtonsToggleable) {
        super(textField, statPanel, progressBar, validateButton, level, repeatButton, exampleSentenceButton,
                scoreLabel, totalScoreLabel, totalAccuracyLabel, festivalButtonsToggleable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void firstStartAction() {
        progressBar.setModel(new DefaultBoundedRangeModel(0, 1, 0, Config.TEST_PER_LEVEL));
        repeatButton.setEnabled(true);
        FestivalWorker ps = new FestivalWorker("Please Spell the word:  " + quizStrings.get(count), festivalButtonsToggleable);
        ps.execute();
        setChanged();
        notifyObservers(quizStrings.get(count));
        VUtil.removeAllListenersFrom(repeatButton);
        repeatButton.addActionListener(new SinglePronounceActionListener(quizStrings.get(count), repeatButton));
        validateButton.setText("Submit");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void firstTryCorrectAction() {
        textField.setText("");
        FestivalWorker ps = new FestivalWorker("correct", count < Config.TEST_PER_LEVEL - 1 ?
                "please spell the word:  " + quizStrings.get(count + 1) : null, validateButton, null, festivalButtonsToggleable);
        WavWorker wavWorker = new WavWorkerWithFestival(Config.getInstance().GAME_CORRECT_AUDIO, ps);
        wavWorker.execute();
        setChanged();
        notifyObservers(count < Config.TEST_PER_LEVEL - 1 ? quizStrings.get(count + 1) : null);
        scoreLabel.setText("Session Score: " + ((VUtil.getLastNumFromString(scoreLabel.getText()) + level * 10)));
        VUtil.getInstance().getUserProfile().increaseScore(level * 10);

        VUtil.getInstance().getManagedWordList().addToMastered(quizStrings.get(count));
        statPanel.correctAt(count);
        statPanel.setWordAt(count, quizStrings.get(count));
        progressBar.setValue(count + 1);

    }

    @Override
    protected void firstTryIncorrectAction() {

        textField.setText("");
        FestivalWorker ps = new FestivalWorker("Incorrect, try once more,"
                + quizStrings.get(count),  quizStrings.get(count), festivalButtonsToggleable);
        WavWorker wavWorker = new WavWorkerWithFestival(Config.getInstance().GAME_WRONG_AUDIO, ps);
        wavWorker.execute();
        statPanel.wrongAt(count);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void secondTryCorrectAction() {

        textField.setText("");

        FestivalWorker ps = new FestivalWorker("correct", count < Config.TEST_PER_LEVEL - 1 ?
                "please spell the word:  " + quizStrings.get(count + 1) : null, festivalButtonsToggleable);
        WavWorker wavWorker = new WavWorkerWithFestival(Config.getInstance().GAME_CORRECT_AUDIO, ps);
        wavWorker.execute();
        setChanged();
        notifyObservers(count < Config.TEST_PER_LEVEL - 1 ? quizStrings.get(count + 1) : null);
        scoreLabel.setText("Session Score: " + ((VUtil.getLastNumFromString(scoreLabel.getText()) + level * 5)));
        VUtil.getInstance().getUserProfile().increaseScore(level * 5);

        statPanel.correctAt(count);
        statPanel.setWordAt(count, quizStrings.get(count));
        progressBar.setValue(count + 1);
        VUtil.getInstance().getManagedWordList().addToFaulted(quizStrings.get(count));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void updateRepeatButton() {
        VUtil.removeAllListenersFrom(repeatButton);
        repeatButton.addActionListener(new SinglePronounceActionListener(quizStrings.get(count), repeatButton));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void bothWrongAction() {

        textField.setText("");

        FestivalWorker ps = new FestivalWorker("Incorrect", count < Config.TEST_PER_LEVEL - 1 ?
                "please spell the word:  " + quizStrings.get(count + 1) : null, festivalButtonsToggleable);
        WavWorker wavWorker = new WavWorkerWithFestival(Config.getInstance().GAME_WRONG_AUDIO, ps);
        wavWorker.execute();
        setChanged();
        notifyObservers(count < Config.TEST_PER_LEVEL - 1 ? quizStrings.get(count + 1) : null);

        statPanel.wrongAt(count);
        statPanel.setWordAt(count, quizStrings.get(count));
        progressBar.setValue(count + 1);

        VUtil.getInstance().getManagedWordList().addToFailed(quizStrings.get(count));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void finished() {

        VUtil.removeAllListenersFrom(validateButton);
        validateButton.setEnabled(false);
        validateButton.setText("Finished");
        repeatButton.setEnabled(false);
        JFrame eolFrame = new JFrame();
        EndOfLevel endOfLevel = new EndOfLevel(level, correctCount >= 9, eolFrame, Config.mode.NEW_QUIZ);
        endOfLevel.updateUI();
        eolFrame.setContentPane(endOfLevel.$$$getRootComponent$$$());
        eolFrame.pack();
        eolFrame.setVisible(true);

        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        eolFrame.setLocation(dimension.width/2-eolFrame.getSize().width/2,
                dimension.height/2-eolFrame.getSize().height/2);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isQuizFinished() {
        return (count < Config.TEST_PER_LEVEL);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initQuizString() {
        quizStrings = VUtil.getInstance().getWordList().getListAtLevel(level);
        Collections.shuffle(quizStrings);
        addObserver(new ExampleSentenceButtonObserver(exampleSentenceButton));
    }
}
