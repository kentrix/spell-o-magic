package models;

import util.wordsManagement.ManagedWordList;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.*;

/**
 * A statistics model for the statistics overview menu
 */
public class StatsTableModel implements TableModel {

    private final ManagedWordList managedWordList;
    private Set<String> stringSet;
    private List<String> stringList;

    public StatsTableModel(ManagedWordList managedWordList) {
        this.managedWordList = managedWordList;
        stringSet = new HashSet<>();
        stringSet.addAll(managedWordList.getFailed().keySet());
        stringSet.addAll(managedWordList.getFaulted().keySet());
        stringSet.addAll(managedWordList.getMastered().keySet());
        stringSet.addAll(managedWordList.getLearned().keySet());
        stringList = new ArrayList<>(stringSet);
        Collections.sort(stringList);
    }

    /**
     * Rebuild the word lists
     */
    public void rebuildTable() {
        stringSet = new HashSet<>();
        stringSet.addAll(managedWordList.getFailed().keySet());
        stringSet.addAll(managedWordList.getFaulted().keySet());
        stringSet.addAll(managedWordList.getMastered().keySet());
        stringSet.addAll(managedWordList.getLearned().keySet());
        stringList = new ArrayList<>(stringSet);
        Collections.sort(stringList);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getRowCount() {
        return stringList.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getColumnCount() {
        return 5;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String getColumnName(int i) {
        switch(i) {
            case 0:
                return "Word";
            case 1:
                return "#Mastered";
            case 2:
                return "#Faulted";
            case 3:
                return "#Failed";
            case 4:
                return "#Learned";
            default:
                return null;
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Class<?> getColumnClass(int i) {
        if (i == 0)
            return String.class;
        else
            return Integer.class;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellEditable(int i, int i1) {
        return false;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public Object getValueAt(int i, int i1) {
        switch (i1) {
            case 0:
                return stringList.get(i);
            case 1:
                return (managedWordList.getMastered().containsKey(stringList.get(i)) ?
                        managedWordList.getMastered().get(stringList.get(i)) : 0);
            case 2:
                return (managedWordList.getFaulted().containsKey(stringList.get(i)) ?
                        managedWordList.getFaulted().get(stringList.get(i)) : 0);
            case 3:
                return (managedWordList.getFailed().containsKey(stringList.get(i)) ?
                        managedWordList.getFailed().get(stringList.get(i)) : 0);
            case 4:
                return (managedWordList.getLearned().containsKey(stringList.get(i)) ?
                        managedWordList.getLearned().get(stringList.get(i)) : 0);
            default:
                return null;
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void setValueAt(Object o, int i, int i1) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addTableModelListener(TableModelListener tableModelListener) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeTableModelListener(TableModelListener tableModelListener) {

    }
}
