package models;

import processHandler.workers.FestivalWorker;
import util.VUtil;

import javax.swing.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Pronounce the example sentence
 * Updates the button with the ability to pronounce the sentence
 */
class ExampleSentenceButtonObserver implements Observer {

    private final JButton exampleSentenceButton;

    public ExampleSentenceButtonObserver(JButton exampleSentenceButton) {
        this.exampleSentenceButton = exampleSentenceButton;
    }

    /**
     * @{inheritDoc}
     */
    @Override
    public void update(Observable observable, Object o) {

        if(o == null) {
            exampleSentenceButton.setEnabled(false);
            return;
        }

        if(observable instanceof QuizModel) {
            if (VUtil.getInstance().getDictionaryMap().containsKey(((String)o).toLowerCase()) &&
                    VUtil.getInstance().getDictionaryMap().get(((String) o).toLowerCase()).containsKey("example")) {
                VUtil.removeAllListenersFrom(exampleSentenceButton);
                exampleSentenceButton.setText("Example");
                exampleSentenceButton.setEnabled(true);
                exampleSentenceButton.addActionListener(e -> {
                    FestivalWorker worker = new FestivalWorker(VUtil.getInstance().getDictionaryMap().get(((String)o).toLowerCase()).get("example"), null,
                            exampleSentenceButton, "Example");
                    worker.execute();
                });
            }
            else {
                exampleSentenceButton.setText("Example N/A");
                exampleSentenceButton.setEnabled(false);
            }
        }

    }
}
