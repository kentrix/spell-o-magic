package gui;

import com.sun.istack.internal.NotNull;
import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.player.MediaPlayer;
import util.Config;

import javax.swing.*;
import java.awt.*;
import java.io.File;

/**
 * Actually panel contains the player component
 */
public class PlayerPanel {
    private JPanel panel;
    private EmbeddedMediaPlayerComponent mediaPlayerComponent;

    private void createUIComponents() {
        mediaPlayerComponent = new EmbeddedMediaPlayerComponent();
        panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(mediaPlayerComponent);
        panel.setVisible(true);

    }

    /**
     * Prepare the media player with the original video
     */
    public void preparePlayerWithDefaultMedia() {
        mediaPlayerComponent.getMediaPlayer().prepareMedia(Config.getInstance().DEFAULT_MEDIA_FILE);
    }

    /**
     * Prepare the media player with the alternative video
     */
    private void preparePlayerWithAltMedia() {
        mediaPlayerComponent.getMediaPlayer().prepareMedia(Config.getInstance().ALT_MEDIA_FILE);
    }

    /**
     * Start the player
     */
    public void startPlayer() {
        UIController.getInstance().updatePlayerSize();
        mediaPlayerComponent.getMediaPlayer().play();
    }

    /**
     * Start the player with the specified path
     *
     * @param path path to the video
     * @deprecated
     */
    public void startPlayer(String path) {
        mediaPlayerComponent.getMediaPlayer().playMedia(new File(path).getPath());
    }

    /**
     * Start the player with the specified file
     *
     * @param file file object of the video
     * @deprecated
     */
    public void startPlayer(File file) {
        mediaPlayerComponent.getMediaPlayer().playMedia(file.getPath());
    }

    /**
     * Pause the player
     */
    public void pausePlayer() {
        mediaPlayerComponent.getMediaPlayer().pause();
    }

    /**
     * Resume the player
     */
    public void resumePlayer() {
        mediaPlayerComponent.getMediaPlayer().play();
    }

    /**
     * Toggle full screen of the player
     */
    public void toggleFullScreen() {
        mediaPlayerComponent.getMediaPlayer().toggleFullScreen();
    }

    /**
     * Mute the player
     */
    public void mutePlayer() {
        mediaPlayerComponent.getMediaPlayer().mute();
    }

    /**
     * Unmute the player
     */
    public void unmutePlayer() {
        mediaPlayerComponent.getMediaPlayer().mute(false);
    }

    /**
     * Set the volume of the player
     *
     * @param i volume to be set to
     */
    public void setPlayerVolume(int i) {
        mediaPlayerComponent.getMediaPlayer().setVolume(i);
    }

    /**
     * Reset the player to its initial state
     * Should be called before each call to startPlayer()
     */
    public void resetPlayer() {
        mediaPlayerComponent.setPreferredSize(panel.getSize());
        MediaPlayer mediaPlayer = mediaPlayerComponent.getMediaPlayer();
        mediaPlayer.setVolume(Config.INITIAL_VOLUME);
        unmutePlayer();
        mediaPlayer.stop();
    }

    /**
     * Stop the player
     */
    public void stopPlayer() {
        mediaPlayerComponent.getMediaPlayer().stop();
    }

    /**
     * Return the player component
     *
     * @return MediaPlayer
     */
    MediaPlayer getPlayer() {
        return mediaPlayerComponent.getMediaPlayer();
    }


    /**
     * Replace the currently playing media with the edited version
     */
    public void replaceWithAltMedia() {
        MediaPlayer mediaPlayer = mediaPlayerComponent.getMediaPlayer();
        long time = mediaPlayer.getTime();
        if (time != -1f) {
            if (mediaPlayer.isPlaying()) {
                //its playing
                mediaPlayer.stop();
                mediaPlayer.prepareMedia(Config.getInstance().ALT_MEDIA_FILE);
                mediaPlayer.play();
                mediaPlayer.setTime(time);
            } else {
                //paused?
                mediaPlayer.stop();
                mediaPlayer.prepareMedia(Config.getInstance().ALT_MEDIA_FILE);
                mediaPlayer.play();
                mediaPlayer.setTime(time);
                mediaPlayer.pause();
            }
        } else {
            preparePlayerWithAltMedia();
        }

    }

    /**
     * Replace the currently playing with the original version
     */
    public void replaceWithOriginalMedia() {

        MediaPlayer mediaPlayer = mediaPlayerComponent.getMediaPlayer();
        long time = mediaPlayer.getTime();
        if (time != -1f) {
            if (mediaPlayer.isPlaying()) {
                //its playing
                mediaPlayer.stop();
                mediaPlayer.prepareMedia(Config.getInstance().DEFAULT_MEDIA_FILE);
                mediaPlayer.play();
                mediaPlayer.setTime(time);
            } else {
                //paused?
                mediaPlayer.stop();
                mediaPlayer.prepareMedia(Config.getInstance().DEFAULT_MEDIA_FILE);
                mediaPlayer.play();
                mediaPlayer.setTime(time);
                mediaPlayer.pause();
            }
        } else {
            preparePlayerWithDefaultMedia();
        }

    }

    /**
     * Process the update event, updates the player's size
     *
     * @param dimension the dimension to resize to
     */
    void updatePlayerSize(Dimension dimension) {
        mediaPlayerComponent.setPreferredSize(dimension);
        mediaPlayerComponent.repaint();
    }

    /**
     * Get the current media timer
     *
     * @return the time the current media is playing in ms
     */
    @NotNull
    double getTime() {
        return mediaPlayerComponent.getMediaPlayer().getTime();
    }

    /**
     * Repaint the component
     */
    public void repaint() {
        panel.repaint();
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel;
    }
}
