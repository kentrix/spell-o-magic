package gui;

import annotations.UnsafeCall;
import com.sun.istack.internal.NotNull;
import processHandler.workers.WavWorker;
import util.ComponentFactory;
import util.Config;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Singleton class which controls the GUI of the program
 */
public class UIController implements Runnable {

    private static UIController uiController = null;
    private Card card;
    //The default card on top is the Main Menu
    private String visibleCard = "MainMenuCard";
    private JFrame rootFrame;

    /**
     * Private constructor for the singleton
     */
    private UIController() {
    }

    /**
     * Get the instance of the UIController
     *
     * @return the UIController singleton instance
     */
    @NotNull
    public static UIController getInstance() {
        if (uiController == null) {
            uiController = new UIController();
        }
        return uiController;
    }

    /**
     * Static method to change the font of the application
     *
     * @param fontUIResource the font to switch to
     */
    public static void setUIFont(javax.swing.plaf.FontUIResource fontUIResource) {

        java.util.Enumeration keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value != null && value instanceof javax.swing.plaf.FontUIResource)
                UIManager.put(key, fontUIResource);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void run() {
        makeGUI();
    }

    /**
     * Build the main GUI and display the JFrame.
     */
    private void makeGUI() {
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        rootFrame = new JFrame();
        rootFrame.setTitle("VOXSPELL");
        rootFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        // Alternative close action
        rootFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                super.windowClosing(windowEvent);
                rootFrame.setVisible(true);
                ComponentFactory.createAndShowQuitDialog();
            }
        });

        // Special listener for resize events as video player does not update accordingly
        rootFrame.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent componentEvent) {
                card.playerContainer.updatePlayerSize(rootFrame.getSize());
            }
        });

        // Fix the size of the frame
        rootFrame.setPreferredSize(new Dimension(1100, 900));
        rootFrame.setMinimumSize(new Dimension(1100, 900));

        card = new Card();
        card.mainMenu.updateUI();

        rootFrame.setContentPane(card.outerPane);
        rootFrame.pack();
        rootFrame.setVisible(true);

        // Center the frame
        rootFrame.setLocation(dimension.width/2-rootFrame.getSize().width/2,
                dimension.height/2-rootFrame.getSize().height/2);

        // Play intro sound
        WavWorker wavWorker = new WavWorker(Config.getInstance().GAME_INTRO_AUDIO);
        wavWorker.execute();

    }

    /**
     * Fixes blank canvas when displaying the video play component
     */
    public void fixMediaPlayerDisplay() {
        // Resize the root and then resize it back
        rootFrame.setPreferredSize(new Dimension(rootFrame.getSize().width -1, rootFrame.getSize().height-1));
        rootFrame.setMinimumSize(new Dimension(rootFrame.getSize().width -1, rootFrame.getSize().height-1));
        rootFrame.setPreferredSize(new Dimension(rootFrame.getSize().width +1, rootFrame.getSize().height+1));
        rootFrame.setMinimumSize(new Dimension(rootFrame.getSize().width +1, rootFrame.getSize().height+1));
        card.playerContainer.updatePlayerSize(new Dimension(rootFrame.getSize().width -1, rootFrame.getSize().height - 1));
    }

    /**
     * Get the root frame of the UIController
     * @return rootFrame
     */
    public JFrame getRootFrame() {
        return rootFrame;
    }

    /**
     * Update the UI of level select menu with a game mode
     * @param mode game mode
     */
    public void updateLevelSelectMenuUI(Config.mode mode) {
        card.levelSelectMenu.updateUI(mode);
    }

    /**
     * Update the UI of player container with a level
     * @param nextLevel next level, this function checks for invalid levels
     */
    public void updatePlayerContainerUIWithLevel(int nextLevel) {
        card.playerContainer.updateUI(nextLevel);
    }

    /**
     * Update the UI of stats overview panel
     */
    public void updateStatsOverviewUI() {
        card.statsOverview.updateUI();
    }

    /**
     * Update the ui plaf trees
     */
    @UnsafeCall("Causes bug where borders around a JComponent disappears after call.")
    @Deprecated
    public void updateUITree() {
        SwingUtilities.updateComponentTreeUI(rootFrame);
    }

    /**
     * Disables the root frame
     */
    public void disableRootFrame() {
        rootFrame.setEnabled(false);
    }

    /**
     * Enables the root frame
     */
    public void enableRootFrame() {
        rootFrame.setEnabled(true);
    }

    /**
     * Update the UI of the settings menu
     */
    public void updateConfigMenuUI() {
        card.configMenu.updateUI();
    }

    /**
     * Update the UI of the quiz panel
     * @param level level to start at
     * @param mode mode of the quiz
     */
    public void updateQuizWithLevel(int level, Config.mode mode) {
        card.quiz.startAtLevel(level, mode);
    }

    /**
     * Update the player size in case of a resize event
     */
    public void updatePlayerSize() {
        card.playerContainer.updatePlayerSize(rootFrame.getSize());
    }

    /**
     * Update the UI of the learning mode
     * @param level the level to start at
     */
    public void updateLearningModeWithLevel(int level) {
        card.learningMode.startAtLevel(level);
    }

    /**
     * Switch to a specific card in the CardLayout of root frame
     * @see CardLayout
     * @param cardName a valid card name
     */
    public void switchCardView(String cardName) {
        this.visibleCard = cardName;
        CardLayout cardLayout = (CardLayout) card.CardPanel.getLayout();
        cardLayout.show(card.CardPanel, cardName);
    }

    /**
     * Shows a help menu depending on the context
     */
    public void showContextualHelp() {
        // Show different text messages depending on the top card
        StringBuilder msg = new StringBuilder();
        switch (visibleCard) {
            case "MainMenuCard":
                msg.append("<html>This is the main menu of VoxSpell <br>");
                msg.append("<p> Here, you can select from the following options </p>");
                msg.append("<ul><li>'Learning Mode': Start the learning mode to learn some new words.</li>");
                msg.append(    "<li>'New Quiz': Start a new quiz and earn some scores.</li>");
                msg.append(    "<li>'Review': Start a new review to have another attempt at failed words.</li>");
                msg.append(    "<li>'Statistics' Check your career scores and statistics.</li>");
                msg.append(    "<li>'Settings' Change the voice/get the definitions/change the wordlist.</li>");
                msg.append(    "<li>'Quit' Quit the application.</li>");
                msg.append("</ul><br>Simply click on the buttons to navigate.</html>");
                break;
            case "LevelSelectCard":
                msg.append("<html>This is the level select menu of VoxSpell <br>");
                msg.append("<p> Here, you can choose which level you want to start your quiz/learning from </p>");
                msg.append("<ul><li>'Learning Mode': Start the learning mode to learn some new words.</li>");
                msg.append(    "<li>'Start': Once you have selected a level, click this to confirm and start.</li>");
                msg.append(    "<li>'Main Menu': Go back to the main menu of VoxSpell.</li>");
                msg.append("</ul><br>Simply click on the buttons to navigate.</html>");
                break;
            case "ConfigMenuCard":
                msg.append("<html>This is the settings menu of VoxSpell <br>");
                msg.append("<p> Here, you can adjust the voice to be used in text-to-speech, ");
                msg.append("change the word lists and obtain the definitions of the words over the internet. <br>");
                msg.append("Reminder to click 'Save Settings' to save you settings! </p>");
                msg.append("<ul><li>'Voice': Change the voice used by text-to-speech. </li>");
                msg.append(    "<li>'Word List': Select your own word list to be used. </li>");
                msg.append(    "<li>'Obtain Definitions': get all the definitions and examples over the network </li>");
                msg.append(    "<li>'Save Settings': Save the settings and go back to main menu. </li>");
                msg.append(    "<li>'Main Menu': Go back to the main menu of VoxSpell without saving the settings.</li>");
                msg.append("</ul><br>Simply click on the buttons to navigate.</html>");
                break;
            case "QuizCard":
                msg.append("<html>This is the Quiz/Review mode for VoxSpell.<br>");
                msg.append("<p> Here, you can test you spelling knowledge and review past mistakes. </p>");
                msg.append("<ul>");
                msg.append("<li>Buttons<ul><li>'Start': To start the quiz. </li>");
                msg.append(    "<li>'Example': Pronounce the example sentence, disabled if none available.</li>");
                msg.append(    "<li>'Repeat': Repeat the last word u hear.</li>");
                msg.append(    "<li>'Main Menu': Go back to the main menu of VoxSpell without saving the settings.</li>");
                msg.append("</ul></li>");
                msg.append("<li> Attempts: Show your attempts and accuracy. </li>");
                msg.append("<li> Voice: Quickly change the voice used. </li>");
                msg.append("</ul>");
                msg.append("<br>Simply click on the buttons to navigate. </html>");
                break;
            case "PlayerCard":
                msg.append("<html>This is the video reward.<br>");
                msg.append("<p> Here, you can play the reward video.</p>");
                msg.append("<br>Simply click on the buttons to navigate. </html>");
                break;
            case "StatsOverviewCard":
                msg.append("<html>This is the statistics view.<br>");
                msg.append("<p> Here, you can view your summary and all the stats.</p>");
                msg.append("<br>Simply click on the buttons to navigate. </html>");
                break;
            case "LearningModeCard":
                msg.append("<html>This is the learning mode for VoxSpell.<br>");
                msg.append("<p> Here, you can learn new words. </p>");
                msg.append("<br>Simply click on the buttons to navigate. </html>");
                break;
            default:
                msg.append("<html>Help menu is yet available for this screen.<br>");
                msg.append("Report this to the developer and try another screen.</html>");
        }
        JOptionPane.showMessageDialog(null, msg);
    }

}
