package gui;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import exceptions.InvalidPushOperationException;
import listeners.SwitchToMainMenuActionListener;
import listeners.SwitchToMainMenuWithConfirmationActionListener;
import models.LearningModel;
import processHandler.workers.FestivalBreakDownWorker;
import processHandler.workers.FestivalWorker;
import util.ComponentFactory;
import util.Config;
import util.VUtil;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

/**
 * Learning mode panel of Voxspell
 */
public class LearningMode implements Observer, FestivalButtonsToggleable {
    private JPanel panel1;
    private JButton submitButton;
    private JButton toggleDefButton;
    private JButton mainMenuButton;
    private JProgressBar progressBar;
    private JLabel wordLabel;
    private JFormattedTextField userInputField;
    private JTextArea exampleLabel;
    private JTextArea defLabel;
    private JButton repeatButton;
    private JButton pronounceDefButton;
    private JButton pronounceEgButton;
    private JButton toggleEgButton;
    private JButton breakDownButton;
    private JButton toggleWordButton;
    private LearnedWordPane learnedWordPane;
    private JComboBox voiceComboBox;
    private int level;

    private boolean isDefHidden;
    private boolean isWordHidden;
    private boolean isEgHidden;
    private boolean prevDefButtonState;
    private boolean prevEgButtonState;
    private boolean isDisableCalled = false;

    private LearningModel model;

    public LearningMode() {
        $$$setupUI$$$();
        mainMenuButton.addActionListener(new SwitchToMainMenuWithConfirmationActionListener());
        userInputField.addActionListener(e ->
                submitButton.doClick());
        toggleDefButton.addActionListener(e -> toggleDef());
        toggleEgButton.addActionListener(e -> toggleEg());
        toggleWordButton.addActionListener(e -> toggleWord());
    }

    /**
     * Start learning mode
     *
     * @param level the level to start at
     */
    public void startAtLevel(int level) {
        updateUI();
        this.level = level;
        this.model = new LearningModel(level, this);

        VUtil.removeAllListenersFrom(submitButton);
        submitButton.addActionListener(e -> model.start());
    }

    /**
     * Initialise the UI
     */
    private void updateUI() {
        // Set all element to initial state
        progressBar.setValue(0);
        userInputField.setText("");
        progressBar.setValue(0);
        wordLabel.setText("");
        submitButton.setText("<html><h1>Start</h1></html>");
        breakDownButton.setEnabled(false);
        repeatButton.setEnabled(false);
        pronounceDefButton.setEnabled(false);
        pronounceEgButton.setEnabled(false);
        toggleDefButton.setEnabled(false);
        toggleWordButton.setEnabled(false);
        toggleEgButton.setEnabled(false);
        prevDefButtonState = false;
        prevEgButtonState = false;
        exampleLabel.setLineWrap(true);
        defLabel.setLineWrap(true);
        learnedWordPane.clear();
        userInputField.requestFocusInWindow();
        hideEg();
        hideWord();
        hideDef();
        voiceComboBox.getModel().setSelectedItem(Config.getInstance().voiceMap.get(Config.getInstance().getVoice()));
    }

    /**
     * Toggle the visibility of the definition label
     */
    private void toggleDef() {
        if (isDefHidden) {
            defLabel.setVisible(true);
        } else
            defLabel.setVisible(false);

        isDefHidden = !isDefHidden;
    }

    /**
     * Toggle the visibility of the example label
     */
    private void toggleEg() {
        if (isEgHidden) {
            exampleLabel.setVisible(true);
        } else
            exampleLabel.setVisible(false);

        isEgHidden = !isEgHidden;
    }

    /**
     * Toggle the visibility of the word label
     */
    private void toggleWord() {

        if (isWordHidden) {
            wordLabel.setVisible(true);
        } else
            wordLabel.setVisible(false);

        isWordHidden = !isWordHidden;
    }

    /**
     * Show the word label
     */
    public void showWord() {
        wordLabel.setVisible(true);
        isWordHidden = false;
    }

    /**
     * Show the example label
     */
    public void showEg() {
        exampleLabel.setVisible(true);
        isEgHidden = false;
    }

    /**
     * Show the definition label
     */
    private void showDef() {
        defLabel.setVisible(true);
        isDefHidden = false;
    }

    /**
     * Hide the definition label
     */
    private void hideDef() {
        defLabel.setVisible(false);
        isDefHidden = true;
    }

    /**
     * Hide the example label
     */
    private void hideEg() {
        exampleLabel.setVisible(false);
        isEgHidden = true;
    }

    /**
     * Hide the word label
     */
    private void hideWord() {
        wordLabel.setVisible(false);
        isWordHidden = true;
    }

    /**
     * Set the word label's text
     *
     * @param word the text to set to
     */
    public void setWord(String word) {
        wordLabel.setText(word);
    }

    /**
     * Set the definition label's text
     *
     * @param def the text to set to
     */
    public void setDef(String def) {
        defLabel.setText(def);
    }

    /**
     * Set the exmaple label's text
     *
     * @param eg the text to set to
     */
    public void setEg(String eg) {
        exampleLabel.setText(eg);
    }

    /**
     * Clear the user input field's text
     */
    public void clearTextField() {
        userInputField.setText("");
    }

    /**
     * Change the submit buttons ActionListener
     */
    public void changeActionListener() {
        VUtil.removeAllListenersFrom(submitButton);
        submitButton.addActionListener(e -> model.validateWord(userInputField.getText()));
        submitButton.setText("Submit");
        toggleDefButton.setEnabled(true);
        toggleWordButton.setEnabled(true);
        toggleEgButton.setEnabled(true);
    }

    /**
     * Update the progress bar with a new value
     *
     * @param i the new value to set to
     */
    public void updateProgress(int i) {
        progressBar.setValue(i);
    }

    /**
     * Push a word to the learned word pane
     *
     * @param word the word to push
     */
    public void pushWordToLearnedPane(String word) {
        try {
            learnedWordPane.pushWord(word);
        } catch (InvalidPushOperationException e) {
            e.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void update(Observable observable, Object o) {
        VUtil.removeAllListenersFrom(pronounceDefButton);
        VUtil.removeAllListenersFrom(pronounceEgButton);
        VUtil.removeAllListenersFrom(breakDownButton);
        VUtil.removeAllListenersFrom(repeatButton);
        // Focus on the text field
        userInputField.requestFocusInWindow();
        showDef();
        hideWord();
        hideEg();
        String word = (String) o;

        // Update all the buttons with their respective listeners
        breakDownButton.addActionListener(e -> {
            SwingWorker worker = new FestivalBreakDownWorker(word, this);
            worker.execute();
        });

        repeatButton.addActionListener(e -> {
            SwingWorker worker = new FestivalWorker(word, null, repeatButton, "Repeat", this);
            worker.execute();
        });
        breakDownButton.setEnabled(true);
        repeatButton.setEnabled(true);
        wordLabel.setText(word.toLowerCase());

        if (VUtil.getInstance().getDictionaryMap().containsKey(word)) {

            pronounceEgButton.setEnabled(true);
            pronounceDefButton.setEnabled(true);

            Map<String, String> sMap = VUtil.getInstance().getDictionaryMap().get(word.toLowerCase());
            defLabel.setText((sMap.containsKey("part_of_speech") ? sMap.get("part_of_speech") + ". " : "")
                    + (sMap.containsKey("definition") ? sMap.get("definition") : ""));

            if (sMap.containsKey("example")) {
                exampleLabel.setText(sMap.get("example"));
            } else {
                exampleLabel.setText("Example Not Available for the word.");
                pronounceEgButton.setEnabled(false);
            }


            pronounceDefButton.addActionListener(e -> {
                if (sMap.containsKey("definition")) {
                    SwingWorker worker = new FestivalWorker(sMap.get("definition"), null,
                            pronounceDefButton, "Listen to Definition", this);
                    worker.execute();
                }
            });

            pronounceEgButton.addActionListener(e -> {
                if (sMap.containsKey("example")) {
                    SwingWorker worker = new FestivalWorker(sMap.get("example"), null,
                            pronounceEgButton, "Listen to Example", this);
                    worker.execute();
                }
            });
        } else {
            defLabel.setText("Definition not Available.");
            exampleLabel.setText("Example not Available");
        }
    }

    /**
     * Finish up the learning mode
     */
    public void finish() {
        // Show a end of level frame
        UIController.getInstance().disableRootFrame();
        JFrame eolFrame = new JFrame();
        EndOfLevel eol = new EndOfLevel(level, false, eolFrame, Config.mode.LEARNING);
        eol.updateUI();
        eolFrame.setContentPane(eol.$$$getRootComponent$$$());
        eolFrame.pack();
        eolFrame.setVisible(true);
        Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        eolFrame.setLocation(dimension.width / 2 - eolFrame.getSize().width / 2,
                dimension.height / 2 - eolFrame.getSize().height / 2);
    }

    /**
     * Need by IDEA UI designer
     */
    private void createUIComponents() {
        voiceComboBox = ComponentFactory.newVoiceSelectionComboBox();
    }

    @Override
    /**
     * @{inheritDoc}
     */
    public void disableAllFestivalButtons() {
        breakDownButton.setEnabled(false);
        if (!isDisableCalled) {
            prevEgButtonState = pronounceEgButton.isEnabled();
            prevDefButtonState = pronounceDefButton.isEnabled();
            isDisableCalled = true;
        }
        pronounceDefButton.setEnabled(false);
        pronounceEgButton.setEnabled(false);
        repeatButton.setEnabled(false);
        submitButton.setEnabled(false);
    }

    @Override
    /**
     * @{inheritDoc}
     */
    public void enableAllFestivalButtons() {
        isDisableCalled = false;
        if (prevDefButtonState) {
            pronounceDefButton.setEnabled(true);
        }
        if (prevEgButtonState) {
            pronounceEgButton.setEnabled(true);
        }
        breakDownButton.setEnabled(true);
        repeatButton.setEnabled(true);
        submitButton.setEnabled(true);

    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        panel1 = new JPanel();
        panel1.setLayout(new FormLayout("fill:max(d;4px):grow(0.8),left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:320px:grow,left:4dlu:noGrow,fill:120px:grow,left:4dlu:noGrow,fill:50px:noGrow,left:4dlu:noGrow,fill:60px:noGrow,left:4dlu:noGrow,fill:60px:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow(0.8)", "center:8px:noGrow,top:4dlu:noGrow,center:max(d;4px):grow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:60px:noGrow,top:4dlu:noGrow,center:60px:noGrow,top:4dlu:noGrow,center:60px:noGrow,top:4dlu:noGrow,center:60px:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:80px:noGrow,top:4dlu:noGrow,center:50dlu:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:8dlu:noGrow"));
        final JLabel label1 = new JLabel();
        label1.setIcon(new ImageIcon(getClass().getResource("/resources/learning.png")));
        label1.setText("");
        CellConstraints cc = new CellConstraints();
        panel1.add(label1, cc.xyw(3, 3, 11, CellConstraints.CENTER, CellConstraints.DEFAULT));
        progressBar = new JProgressBar();
        progressBar.setIndeterminate(false);
        progressBar.setMaximum(10);
        progressBar.setStringPainted(true);
        progressBar.setValue(0);
        panel1.add(progressBar, cc.xyw(3, 19, 11, CellConstraints.FILL, CellConstraints.DEFAULT));
        final JLabel label2 = new JLabel();
        label2.setText("Progress");
        panel1.add(label2, cc.xyw(5, 17, 2, CellConstraints.LEFT, CellConstraints.DEFAULT));
        userInputField = new JFormattedTextField();
        userInputField.setFont(new Font(userInputField.getFont().getName(), userInputField.getFont().getStyle(), 48));
        userInputField.setHorizontalAlignment(0);
        panel1.add(userInputField, cc.xyw(3, 23, 5, CellConstraints.FILL, CellConstraints.FILL));
        toggleWordButton = new JButton();
        toggleWordButton.setText("<html>Show/Hide Word</html>");
        panel1.add(toggleWordButton, cc.xywh(9, 5, 5, 3, CellConstraints.FILL, CellConstraints.FILL));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new FormLayout("fill:d:grow", "center:d:grow"));
        panel1.add(panel2, cc.xywh(3, 9, 5, 3, CellConstraints.DEFAULT, CellConstraints.FILL));
        panel2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Definition", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, new Color(-16777216)));
        defLabel = new JTextArea();
        defLabel.setBackground(new Color(-1184275));
        defLabel.setText("");
        panel2.add(defLabel, cc.xy(1, 1, CellConstraints.DEFAULT, CellConstraints.CENTER));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new FormLayout("fill:d:grow", "center:d:grow"));
        panel1.add(panel3, cc.xywh(3, 13, 5, 3, CellConstraints.DEFAULT, CellConstraints.FILL));
        panel3.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Example", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, new Color(-16777216)));
        exampleLabel = new JTextArea();
        exampleLabel.setBackground(new Color(-1184275));
        exampleLabel.setText("");
        panel3.add(exampleLabel, cc.xy(1, 1));
        repeatButton = new JButton();
        repeatButton.setIcon(new ImageIcon(getClass().getResource("/com/alee/extended/filechooser/icons/refresh.png")));
        repeatButton.setText("Repeat");
        panel1.add(repeatButton, cc.xy(7, 5));
        breakDownButton = new JButton();
        breakDownButton.setText("<html>Break Down</html>");
        panel1.add(breakDownButton, cc.xy(7, 7));
        learnedWordPane = new LearnedWordPane();
        panel1.add(learnedWordPane.$$$getRootComponent$$$(), cc.xyw(3, 21, 11, CellConstraints.DEFAULT, CellConstraints.FILL));
        pronounceEgButton = new JButton();
        pronounceEgButton.setText("<html>Listen to Example</html>");
        panel1.add(pronounceEgButton, cc.xyw(9, 13, 5));
        toggleEgButton = new JButton();
        toggleEgButton.setText("<html>Show/Hide Example</html>");
        panel1.add(toggleEgButton, cc.xyw(9, 15, 5));
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new FormLayout("fill:d:grow", "center:d:grow"));
        panel1.add(panel4, cc.xywh(3, 5, 3, 3, CellConstraints.FILL, CellConstraints.FILL));
        panel4.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Word", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font(panel4.getFont().getName(), panel4.getFont().getStyle(), panel4.getFont().getSize()), new Color(-16777216)));
        wordLabel = new JLabel();
        wordLabel.setFont(new Font(wordLabel.getFont().getName(), wordLabel.getFont().getStyle(), 28));
        wordLabel.setText("word");
        panel4.add(wordLabel, cc.xy(1, 1, CellConstraints.CENTER, CellConstraints.DEFAULT));
        mainMenuButton = new JButton();
        mainMenuButton.setIcon(new ImageIcon(getClass().getResource("/com/alee/managers/style/icons/component/menu.png")));
        mainMenuButton.setText("<html>Main Menu</html>");
        panel1.add(mainMenuButton, cc.xyw(9, 25, 5));
        panel1.add(voiceComboBox, cc.xyw(5, 25, 3));
        submitButton = new JButton();
        submitButton.setText("Submit");
        panel1.add(submitButton, cc.xyw(9, 23, 5, CellConstraints.DEFAULT, CellConstraints.FILL));
        final JLabel label3 = new JLabel();
        label3.setText("Voice");
        panel1.add(label3, cc.xy(3, 25));
        toggleDefButton = new JButton();
        toggleDefButton.setText("<html>Show/Hide Definition</html>");
        panel1.add(toggleDefButton, cc.xyw(9, 11, 5));
        pronounceDefButton = new JButton();
        pronounceDefButton.setText("<html>Listen to Definition</html>");
        panel1.add(pronounceDefButton, cc.xyw(9, 9, 5, CellConstraints.DEFAULT, CellConstraints.CENTER));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel1;
    }
}
