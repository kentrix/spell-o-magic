package gui;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import listeners.*;
import util.Config;
import util.VUtil;

import javax.swing.*;

/**
 * End of level dialog
 */
public class EndOfLevel {
    private JPanel panel1;
    private JButton nextLevelButton;
    private JButton playVidButton;
    private JButton mainMenubutton;
    private JButton repeatLevelButton;
    private JLabel messageLabel;
    private final int curLevel;
    private final boolean playVideo;
    private final JFrame frame;
    private final Config.mode mode;

    public EndOfLevel(int curLevel, boolean playVideo, JFrame frame, Config.mode mode) {
        this.curLevel = curLevel;
        this.frame = frame;
        this.playVideo = playVideo;
        this.mode = mode;
    }

    /**
     * Needed by IDEA UI Designer
     * Create the UIComponents
     */
    private void createUIComponents() {
        nextLevelButton = new JButton();
        playVidButton = new JButton();
        mainMenubutton = new JButton();
        repeatLevelButton = new JButton();

    }


    /**
     * Update the UI of the config menu, should be called each time before displaying this dialog
     */
    public void updateUI() {
        nextLevelButton.setText("<html>Next Level</html>");
        nextLevelButton.setEnabled(true);
        repeatLevelButton.setText("<html>Repeat Level</html>");
        repeatLevelButton.setEnabled(true);

        if (mode == Config.mode.NEW_QUIZ) {
            // We get here from finishing a quiz level
            repeatLevelButton.addActionListener(new SwitchToQuizModeActionListener() {
                @Override
                public void extraAction() {
                    UIController.getInstance().updateQuizWithLevel(curLevel, Config.mode.NEW_QUIZ);
                    UIController.getInstance().enableRootFrame();
                    frame.dispose();
                }
            });

            if ((curLevel < VUtil.getInstance().getWordList().getLevelCount())) {
                nextLevelButton.addActionListener(new SwitchToQuizModeActionListener() {
                    @Override
                    public void extraAction() {
                        UIController.getInstance().updateQuizWithLevel(curLevel + 1, Config.mode.NEW_QUIZ);
                        UIController.getInstance().enableRootFrame();
                        frame.dispose();
                    }
                });
            } else {
                // Level max
                nextLevelButton.addActionListener(e -> JOptionPane.showMessageDialog(null, "<html>You are already at max level!</html>"));
            }
        } else if (mode == Config.mode.LEARNING) {
            repeatLevelButton.addActionListener(new SwitchToLearningModeActionListener() {
                @Override
                public void extraAction() {
                    UIController.getInstance().updateLearningModeWithLevel(curLevel);
                    UIController.getInstance().enableRootFrame();
                    frame.dispose();
                }
            });
            if ((curLevel < VUtil.getInstance().getWordList().getLevelCount())) {
                nextLevelButton.addActionListener(new SwitchToLearningModeActionListener() {
                    @Override
                    public void extraAction() {
                        UIController.getInstance().updateLearningModeWithLevel(curLevel + 1);
                        UIController.getInstance().enableRootFrame();
                        frame.dispose();
                    }
                });
            } else {
                //level max
                nextLevelButton.addActionListener(e -> JOptionPane.showMessageDialog(null, "<html>You are already at max level!</html>"));
            }
        } else if (mode == Config.mode.REVIEW) {
            //We get here from finishing a review
            nextLevelButton.setEnabled(false);
            repeatLevelButton.setText("<html>Review again</html>");

            repeatLevelButton.addActionListener(new SwitchToReviewModeActionListener() {
                @Override
                public void extraAction() {
                    UIController.getInstance().enableRootFrame();
                    frame.dispose();
                }
            });
        }


        playVidButton.setText("<html>Play a Video</html>");
        if (playVideo) {
            messageLabel.setText("<html> You finished the level! <br>"
                    + "You did well! You can get the video reward. </html>");
            UIController.getInstance().updatePlayerContainerUIWithLevel(
                    mode == Config.mode.NEW_QUIZ ?
                            (curLevel < VUtil.getInstance().getWordList().getLevelCount() ? curLevel + 1 : 0) : 0);
            playVidButton.addActionListener(new SwitchToMediaPlayerActionListener() {
                @Override
                public void extraAction() {
                    UIController.getInstance().enableRootFrame();
                    frame.dispose();
                }
            });
        } else {
            playVidButton.setEnabled(false);
            switch (mode) {
                // Different message for different mode and correct count
                case LEARNING:
                    messageLabel.setText("<html> You finished the learning mode! <br>"
                            + "Try playing the quiz mode or review mode to get a video reward. </html>");
                    break;
                case NEW_QUIZ:
                    messageLabel.setText("<html> You finished the quiz mode! <br>" +
                            "Sadly, you got less than 9 words correct, video reward is disabled.</html>");
                    break;
                case REVIEW:
                    messageLabel.setText("<html> You finished the review mode! <br>" +
                            "Sadly, you did not get all of the words correct, video reward is disabled. </html>");
            }
        }

        mainMenubutton.setText("<html>Main Menu</html>");
        mainMenubutton.addActionListener(new SwitchToMainMenuActionListener() {
            @Override
            public void extraAction() {
                UIController.getInstance().enableRootFrame();
                frame.dispose();
            }
        });
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        panel1 = new JPanel();
        panel1.setLayout(new FormLayout("fill:max(d;4px):noGrow,left:4dlu:noGrow,fill:d:grow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:max(d;4px):noGrow", "center:max(d;4px):noGrow,top:4dlu:noGrow,center:d:grow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow"));
        messageLabel = new JLabel();
        messageLabel.setIcon(new ImageIcon(getClass().getResource("/com/alee/managers/notification/icons/types/information.png")));
        messageLabel.setText("You finished the level!");
        CellConstraints cc = new CellConstraints();
        panel1.add(messageLabel, cc.xyw(3, 3, 7, CellConstraints.CENTER, CellConstraints.DEFAULT));
        nextLevelButton.setText("Button");
        panel1.add(nextLevelButton, cc.xy(3, 5));
        playVidButton.setText("Button");
        panel1.add(playVidButton, cc.xy(7, 5));
        mainMenubutton.setText("Button");
        panel1.add(mainMenubutton, cc.xy(9, 5));
        repeatLevelButton.setText("Button");
        panel1.add(repeatLevelButton, cc.xy(5, 5));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel1;
    }
}
