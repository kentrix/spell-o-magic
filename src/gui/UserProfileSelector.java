package gui;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import profiles.AddNewUserProfile;
import profiles.GuestUserProfile;
import profiles.UserProfile;
import util.Config;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.FontUIResource;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;
import java.util.stream.Collectors;

/**
 * User Profile selector
 */
public class UserProfileSelector extends Observable {
    private final List<UserProfile> userProfiles;
    private JPanel jPanel;
    private JComboBox profileComboBox;
    private JButton OKButton;
    private JButton quitButton;
    private JLabel masteredLabel;
    private JLabel faultedLabel;
    private JLabel failedLabel;
    private JButton deleteProfileButton;
    private JLabel learnedLabel;
    private JLabel scoreLabel;
    private JLabel accuracyLabel;
    private JLabel fontSizeLabel;
    private JSpinner fontSizeSpinner;
    private JLabel exampleTextLabel;
    private JComboBox fontComboBox;
    private JLabel fontLabel;
    private JPanel summaryCardPanel;

    public UserProfileSelector(List<UserProfile> userProfiles) {
        this.userProfiles = userProfiles;
        $$$setupUI$$$();
        OKButton.addActionListener(e -> {
            setChanged();
            notifyObservers(profileComboBox.getSelectedItem());
        });

        profileComboBox.addActionListener(e -> {
            if (profileComboBox.getSelectedItem() instanceof AddNewUserProfile) {
                for (; ; ) {
                    String input = JOptionPane.showInputDialog("User Name:");
                    if (input == null) {
                        break;
                    }
                    boolean dup = false;
                    for (UserProfile p :
                            userProfiles) {
                        if (p.getUserName().equalsIgnoreCase(input)) {
                            dup = true;
                            break;
                        }
                    }
                    if (!dup) {
                        UserProfile newUserProfile = new UserProfile(input);
                        setChanged();
                        notifyObservers(newUserProfile);
                        break;
                    } else {
                        JOptionPane.showMessageDialog(null, "Username already exists. Try Another.");
                    }
                }
            } else if (profileComboBox.getSelectedItem() instanceof GuestUserProfile) {
                CardLayout cardLayout = (CardLayout) summaryCardPanel.getLayout();
                cardLayout.show(summaryCardPanel, "WarningCard");
            } else if (profileComboBox.getSelectedItem() instanceof UserProfile) {

                CardLayout cardLayout = (CardLayout) summaryCardPanel.getLayout();
                cardLayout.show(summaryCardPanel, "SummaryCard");

                UserProfile profile = (UserProfile) profileComboBox.getSelectedItem();
                masteredLabel.setText(profile.getMasteredCount() + " times");
                faultedLabel.setText(profile.getFaultedCount() + " times");
                failedLabel.setText(profile.getFailedCount() + " times");
                learnedLabel.setText(profile.getLearnedCount() + " times");
                scoreLabel.setText(profile.getScore() + "");
                accuracyLabel.setText(String.format("%.2f%%", profile.getAccuracy() * 100));
            }
        });
        quitButton.addActionListener(e -> System.exit(1));
        deleteProfileButton.addActionListener(actionEvent -> {
            UserProfile selectedProfile = (UserProfile) profileComboBox.getModel().getSelectedItem();
            if (!(selectedProfile).getUserName().equalsIgnoreCase("Guest")
                    && !(selectedProfile).getUserName().equalsIgnoreCase("New User...")) {
                int userOpt = JOptionPane.showConfirmDialog(null, "Are you sure you want to delete the profile of " +
                        (selectedProfile).getUserName() + "?");
                if (userOpt == JOptionPane.YES_OPTION) {
                    Config.getInstance().queueProfileRemoval(selectedProfile);
                    profileComboBox.removeItem(profileComboBox.getModel().getSelectedItem());
                }
            } else {
                JOptionPane.showMessageDialog(null, "Sorry, you cant delete this profile.");
            }
        });
    }

    /**
     * Needed by IDEA UI designer
     * Create UI Components
     */
    private void createUIComponents() {
        profileComboBox = new JComboBox();
        summaryCardPanel = new JPanel();
        fontSizeSpinner = new JSpinner();
        fontSizeSpinner.setModel(new SpinnerNumberModel(Config.getInstance().getFontSize(), Config.MIN_FONT_SIZE,
                Config.MAX_FONT_SIZE, 1));
        fontSizeSpinner.addChangeListener(e -> {
            /*
            UIController.setUIFont(new FontUIResource(Config.getInstance().getFont(),
                    Font.PLAIN, (Integer) fontSizeSpinner.getModel().getValue()));
                    */
            Config.getInstance().setFontSize((Integer) fontSizeSpinner.getModel().getValue());
            exampleTextLabel.setFont(new FontUIResource((String) fontComboBox.getModel().getSelectedItem(),
                    Font.PLAIN, (Integer) fontSizeSpinner.getModel().getValue()));
        });
        JComponent editor = new JSpinner.NumberEditor(fontSizeSpinner);
        fontSizeSpinner.setEditor(editor);

        List<Font> fl = Arrays.asList(GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts());
        ArrayList<String> fontList = fl.stream().map(Font::getFontName).collect(Collectors.toCollection(ArrayList::new));
        fontComboBox = new JComboBox(fontList.toArray());
        fontComboBox.getModel().setSelectedItem(Config.getInstance().getFont());
        fontComboBox.addActionListener(e -> {
            /*
            UIController.setUIFont(new FontUIResource((String) fontComboBox.getModel().getSelectedItem(),
                    Font.PLAIN, (Integer) fontSizeSpinner.getModel().getValue()));
            */
            Config.getInstance().setFont((String) fontComboBox.getModel().getSelectedItem());
            exampleTextLabel.setFont(new FontUIResource((String) fontComboBox.getModel().getSelectedItem(),
                    Font.PLAIN, (Integer) fontSizeSpinner.getModel().getValue()));
        });
    }

    /**
     * Update the UI of user profile selector
     */
    public void updateUI() {

        UserProfile profile = userProfiles.get(0);
        masteredLabel.setText(profile.getMasteredCount() + " times");
        faultedLabel.setText(profile.getFaultedCount() + " times");
        failedLabel.setText(profile.getFailedCount() + " times");
        learnedLabel.setText(profile.getLearnedCount() + " times");
        scoreLabel.setText(profile.getScore() + "");
        accuracyLabel.setText(String.format("%.2f%%", profile.getAccuracy() * 100));

        CardLayout cardLayout = (CardLayout) summaryCardPanel.getLayout();
        cardLayout.show(summaryCardPanel, "WarningCard");

        profileComboBox.setModel(new DefaultComboBoxModel(userProfiles.toArray()) {
        });
    }

    /**
     * Get the content pane
     *
     * @return
     */
    public JPanel getContentPane() {
        return jPanel;
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        jPanel = new JPanel();
        jPanel.setLayout(new FormLayout("fill:max(d;4px):grow,left:4dlu:noGrow,fill:120px:noGrow,left:4dlu:noGrow,fill:120px:noGrow,left:4dlu:noGrow,fill:120px:noGrow,left:4dlu:noGrow,fill:max(d;4px):noGrow,left:4dlu:noGrow,fill:max(d;4px):grow", "center:max(d;4px):grow,top:4dlu:noGrow,center:d:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):grow"));
        final JLabel label1 = new JLabel();
        label1.setText("Profile");
        CellConstraints cc = new CellConstraints();
        jPanel.add(label1, cc.xy(3, 3));
        jPanel.add(profileComboBox, cc.xyw(3, 5, 5));
        deleteProfileButton = new JButton();
        deleteProfileButton.setText("Delete Profile");
        jPanel.add(deleteProfileButton, cc.xy(9, 5));
        quitButton = new JButton();
        quitButton.setText("Quit");
        jPanel.add(quitButton, cc.xy(7, 13));
        OKButton = new JButton();
        OKButton.setText("OK");
        jPanel.add(OKButton, cc.xy(5, 13));
        fontSizeLabel = new JLabel();
        fontSizeLabel.setIcon(new ImageIcon(getClass().getResource("/com/alee/extended/language/icons/text.png")));
        fontSizeLabel.setText("Font Size");
        jPanel.add(fontSizeLabel, cc.xy(3, 7));
        jPanel.add(fontSizeSpinner, cc.xyw(5, 7, 3, CellConstraints.FILL, CellConstraints.DEFAULT));
        jPanel.add(fontComboBox, cc.xyw(5, 9, 3));
        fontLabel = new JLabel();
        fontLabel.setIcon(new ImageIcon(getClass().getResource("/com/sun/javafx/scene/web/skin/FontColor_16x16_JFX.png")));
        fontLabel.setText("Font");
        jPanel.add(fontLabel, cc.xy(3, 9));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new FormLayout("fill:d:grow", "center:d:grow"));
        jPanel.add(panel1, cc.xywh(9, 7, 1, 3));
        panel1.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, new Color(-4473925)));
        exampleTextLabel = new JLabel();
        exampleTextLabel.setText("Abc123");
        panel1.add(exampleTextLabel, cc.xy(1, 1, CellConstraints.CENTER, CellConstraints.DEFAULT));
        summaryCardPanel.setLayout(new CardLayout(0, 0));
        jPanel.add(summaryCardPanel, cc.xyw(3, 11, 7));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new FormLayout("fill:d:grow,left:4dlu:noGrow,fill:d:grow,left:4dlu:noGrow,fill:d:grow,left:4dlu:noGrow,fill:max(d;4px):grow", "center:max(d;4px):grow,top:4dlu:noGrow,center:max(d;4px):grow"));
        summaryCardPanel.add(panel2, "SummaryCard");
        panel2.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Profile Summary", TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION, new Font(panel2.getFont().getName(), panel2.getFont().getStyle(), panel2.getFont().getSize()), new Color(-16777216)));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new FormLayout("fill:d:noGrow", "center:max(d;4px):noGrow"));
        panel2.add(panel3, cc.xy(1, 1));
        panel3.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Mastered", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, new Color(-16777216)));
        masteredLabel = new JLabel();
        masteredLabel.setText("Label");
        panel3.add(masteredLabel, cc.xy(1, 1));
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new FormLayout("fill:d:grow", "center:d:grow"));
        panel2.add(panel4, cc.xy(3, 1));
        panel4.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Faulted", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, new Color(-16777216)));
        faultedLabel = new JLabel();
        faultedLabel.setText("Label");
        panel4.add(faultedLabel, cc.xy(1, 1));
        final JPanel panel5 = new JPanel();
        panel5.setLayout(new FormLayout("fill:d:grow", "center:d:grow"));
        panel2.add(panel5, cc.xy(5, 1));
        panel5.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Failed", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, new Color(-16777216)));
        failedLabel = new JLabel();
        failedLabel.setText("Label");
        panel5.add(failedLabel, cc.xy(1, 1));
        final JPanel panel6 = new JPanel();
        panel6.setLayout(new FormLayout("fill:d:grow", "center:d:grow"));
        panel2.add(panel6, cc.xy(7, 1));
        panel6.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Learned", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, new Color(-16777216)));
        learnedLabel = new JLabel();
        learnedLabel.setText("Label");
        panel6.add(learnedLabel, cc.xy(1, 1));
        final JPanel panel7 = new JPanel();
        panel7.setLayout(new FormLayout("fill:d:grow", "center:d:grow"));
        panel2.add(panel7, cc.xyw(1, 3, 3));
        panel7.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Total Score", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, new Color(-16777216)));
        scoreLabel = new JLabel();
        scoreLabel.setText("Label");
        panel7.add(scoreLabel, cc.xy(1, 1));
        final JPanel panel8 = new JPanel();
        panel8.setLayout(new FormLayout("fill:d:grow", "center:d:grow"));
        panel2.add(panel8, cc.xyw(5, 3, 3));
        panel8.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Accuracy", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, new Color(-16777216)));
        accuracyLabel = new JLabel();
        accuracyLabel.setText("Label");
        panel8.add(accuracyLabel, cc.xy(1, 1));
        final JPanel panel9 = new JPanel();
        panel9.setLayout(new FormLayout("fill:d:grow", "center:d:grow"));
        summaryCardPanel.add(panel9, "WarningCard");
        panel9.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Warning", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, new Color(-16777216)));
        final JLabel label2 = new JLabel();
        label2.setIcon(new ImageIcon(getClass().getResource("/com/alee/extended/style/icons/status/warn.png")));
        label2.setText("<html>Guest Profile does not have any progression, you cannot save them.<br> Create or select a profile to save your progression.</html>");
        panel9.add(label2, cc.xy(1, 1, CellConstraints.CENTER, CellConstraints.DEFAULT));
        fontLabel.setLabelFor(fontComboBox);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return jPanel;
    }
}
