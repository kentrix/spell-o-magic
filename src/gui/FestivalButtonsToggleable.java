package gui;

/**
 * This interface should be implemented by GUI classes which can disables/enables
 * all button which would trigger a new Festival process.
 *
 * Prevents Festival from saying two or more things at once.
 */
public interface FestivalButtonsToggleable {
    /**
     * Disable all buttons that trigger a festival tts
     */
    void disableAllFestivalButtons();

    /**
     * Enable all buttons that trigger a festival tts
     */
    void enableAllFestivalButtons();
}
