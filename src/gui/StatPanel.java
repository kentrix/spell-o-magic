package gui;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import exceptions.InvalidPushOperationException;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by you on 7/09/2016.
 */
public class StatPanel {
    private JPanel statsRootPane;
    private JLabel wordNumberLabel1;
    private JLabel wordNumberLabel2;
    private JLabel wordNumberLabel3;
    private JLabel wordNumberLabel4;
    private JLabel wordNumberLabel5;
    private JLabel wordNumberLabel6;
    private JLabel wordNumberLabel7;
    private JLabel wordNumberLabel8;
    private JLabel wordNumberLabel9;
    private JLabel wordNumberLabel10;
    private AccuracyPane accuracyPane1;
    private AccuracyPane accuracyPane2;
    private AccuracyPane accuracyPane3;
    private AccuracyPane accuracyPane4;
    private AccuracyPane accuracyPane5;
    private AccuracyPane accuracyPane6;
    private AccuracyPane accuracyPane7;
    private AccuracyPane accuracyPane8;
    private AccuracyPane accuracyPane9;
    private AccuracyPane accuracyPane10;
    private JLabel accuracyPercentileLabel;
    private JSeparator sep1;
    private JSeparator sep2;
    private JSeparator sep3;
    private JSeparator sep4;
    private JSeparator sep5;
    private JSeparator sep6;
    private JSeparator sep7;
    private JSeparator sep8;
    private JSeparator sep9;
    private JLabel wordLabel1;
    private JLabel wordLabel2;
    private JLabel wordLabel3;
    private JLabel wordLabel4;
    private JLabel wordLabel5;
    private JLabel wordLabel6;
    private JLabel wordLabel7;
    private JLabel wordLabel8;
    private JLabel wordLabel9;
    private JLabel wordLabel10;

    private List<JSeparator> separatorList;
    private List<JLabel> labelList;
    private Map<JLabel, AccuracyPane> accuracyPaneMap;
    private List<JLabel> wordLabelList;

    private int correctCount = 0;
    private int wrongCount = 0;

    private void createUIComponents() {
        //The following are needed for the UI designer to work
        wordNumberLabel1 = new JLabel();
        wordNumberLabel2 = new JLabel();
        wordNumberLabel3 = new JLabel();
        wordNumberLabel4 = new JLabel();
        wordNumberLabel5 = new JLabel();
        wordNumberLabel6 = new JLabel();
        wordNumberLabel7 = new JLabel();
        wordNumberLabel8 = new JLabel();
        wordNumberLabel9 = new JLabel();
        wordNumberLabel10 = new JLabel();

        accuracyPane1 = new AccuracyPane();
        accuracyPane2 = new AccuracyPane();
        accuracyPane3 = new AccuracyPane();
        accuracyPane4 = new AccuracyPane();
        accuracyPane5 = new AccuracyPane();
        accuracyPane6 = new AccuracyPane();
        accuracyPane7 = new AccuracyPane();
        accuracyPane8 = new AccuracyPane();
        accuracyPane9 = new AccuracyPane();
        accuracyPane10 = new AccuracyPane();

        labelList = new ArrayList<>();
        labelList.add(wordNumberLabel1);
        labelList.add(wordNumberLabel2);
        labelList.add(wordNumberLabel3);
        labelList.add(wordNumberLabel4);
        labelList.add(wordNumberLabel5);
        labelList.add(wordNumberLabel6);
        labelList.add(wordNumberLabel7);
        labelList.add(wordNumberLabel8);
        labelList.add(wordNumberLabel9);
        labelList.add(wordNumberLabel10);

        accuracyPaneMap = new HashMap<>();
        accuracyPaneMap.put(labelList.get(0), accuracyPane1);
        accuracyPaneMap.put(labelList.get(1), accuracyPane2);
        accuracyPaneMap.put(labelList.get(2), accuracyPane3);
        accuracyPaneMap.put(labelList.get(3), accuracyPane4);
        accuracyPaneMap.put(labelList.get(4), accuracyPane5);
        accuracyPaneMap.put(labelList.get(5), accuracyPane6);
        accuracyPaneMap.put(labelList.get(6), accuracyPane7);
        accuracyPaneMap.put(labelList.get(7), accuracyPane8);
        accuracyPaneMap.put(labelList.get(8), accuracyPane9);
        accuracyPaneMap.put(labelList.get(9), accuracyPane10);

        sep1 = new JSeparator();
        sep2 = new JSeparator();
        sep3 = new JSeparator();
        sep4 = new JSeparator();
        sep5 = new JSeparator();
        sep6 = new JSeparator();
        sep7 = new JSeparator();
        sep8 = new JSeparator();
        sep9 = new JSeparator();

        separatorList = new ArrayList<>();
        separatorList.add(sep1);
        separatorList.add(sep2);
        separatorList.add(sep3);
        separatorList.add(sep4);
        separatorList.add(sep5);
        separatorList.add(sep6);
        separatorList.add(sep7);
        separatorList.add(sep8);
        separatorList.add(sep9);

        accuracyPercentileLabel = new JLabel();
        accuracyPercentileLabel.setHorizontalAlignment(JLabel.CENTER);
        accuracyPercentileLabel.setVerticalAlignment(JLabel.CENTER);

        wordLabel1 = new JLabel();
        wordLabel2 = new JLabel();
        wordLabel3 = new JLabel();
        wordLabel4 = new JLabel();
        wordLabel5 = new JLabel();
        wordLabel6 = new JLabel();
        wordLabel7 = new JLabel();
        wordLabel8 = new JLabel();
        wordLabel9 = new JLabel();
        wordLabel10 = new JLabel();

        wordLabelList = new ArrayList<>();
        wordLabelList.add(wordLabel1);
        wordLabelList.add(wordLabel2);
        wordLabelList.add(wordLabel3);
        wordLabelList.add(wordLabel4);
        wordLabelList.add(wordLabel5);
        wordLabelList.add(wordLabel6);
        wordLabelList.add(wordLabel7);
        wordLabelList.add(wordLabel8);
        wordLabelList.add(wordLabel9);
        wordLabelList.add(wordLabel10);

        statsRootPane = new JPanel();
    }

    /**
     * Add a tick to the accuracy panel at the index
     * and increase the correct count
     *
     * @param index the index to add the tick
     */
    public void correctAt(int index) {
        correctCount++;
        try {
            accuracyPaneMap.get(labelList.get(index)).pushTick();
        } catch (InvalidPushOperationException e) {
            e.printStackTrace();
        }
        updateUI();
    }


    /**
     * Add a cross to the accuracy panel at the index
     * and increase the internal wrong count
     *
     * @param index the index to add the cross
     */
    public void wrongAt(int index) {
        wrongCount++;
        try {
            accuracyPaneMap.get(labelList.get(index)).pushCross();
        } catch (InvalidPushOperationException e) {
            e.printStackTrace();
        }
        updateUI();
    }

    /**
     * Update the statistics panel UI, to update the accuracy percentage
     */
    public void updateUI() {
        if (correctCount == 0 && wrongCount == 0) {
            accuracyPercentileLabel.setText("Session Accuracy: --%");
        } else {
            accuracyPercentileLabel.setText(String.format("Session Accuracy %.2f %%", (((double) correctCount) / (correctCount + wrongCount) * 100)));
        }
        statsRootPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(-16777216)), "Attempts", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION, new Font(statsRootPane.getFont().getName(), statsRootPane.getFont().getStyle(), statsRootPane.getFont().getSize()), new Color(-16777216)));
    }

    /**
     * Return the statistics panel to its initial state
     */
    public void clear() {

        for (AccuracyPane p : accuracyPaneMap.values()) {
            p.$$$getRootComponent$$$().setVisible(true);
            p.clear();
        }

        for (JLabel l : labelList) {
            l.setVisible(true);
        }
        for (JSeparator s : separatorList) {
            s.setVisible(true);
        }

        for (JLabel wl : wordLabelList) {
            wl.setText("");
        }
        correctCount = 0;
        wrongCount = 0;

    }

    /**
     * Hide unused accuracy in case of having less than 10 words to quiz on
     *
     * @param count the number of panes to hide
     */
    public void hideUnusedAccuracyPanes(int count) {
        if (count <= 0 || count >= 10) {
            return;
        }
        int end = 9;
        while (count > 0) {
            labelList.get(end).setVisible(false);
            separatorList.get(end > 0 ? end - 1 : end).setVisible(false);
            count--;
            end--;
        }
        statsRootPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(-16777216)), "Attempts", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION, new Font(statsRootPane.getFont().getName(), statsRootPane.getFont().getStyle(), statsRootPane.getFont().getSize()), new Color(-16777216)));
        statsRootPane.setVisible(true);
    }

    /**
     * Set the word displaying at the accuracy pane
     *
     * @param i    the index of the pane
     * @param word the word to set to
     */
    public void setWordAt(int i, String word) {
        wordLabelList.get(i).setText(word);
    }


    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        statsRootPane.setLayout(new FormLayout("fill:max(d;4px):noGrow,left:4dlu:noGrow,fill:d:grow,left:4dlu:noGrow,fill:4px:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:4px:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:4px:noGrow,left:4dlu:noGrow,center:d:grow,left:4dlu:noGrow,fill:4dlu:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:4dlu:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:4dlu:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:4dlu:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:4dlu:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:4dlu:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:max(d;4px):noGrow", "center:max(d;4px):grow,top:4dlu:noGrow,center:30px:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:20dlu:noGrow,top:4dlu:noGrow,center:max(d;4px):grow"));
        statsRootPane.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(-16777216)), "Attempts", TitledBorder.CENTER, TitledBorder.DEFAULT_POSITION, new Font(statsRootPane.getFont().getName(), statsRootPane.getFont().getStyle(), statsRootPane.getFont().getSize()), new Color(-16777216)));
        wordNumberLabel1.setText("1.");
        CellConstraints cc = new CellConstraints();
        statsRootPane.add(wordNumberLabel1, cc.xy(3, 3, CellConstraints.CENTER, CellConstraints.DEFAULT));
        statsRootPane.add(accuracyPane1.$$$getRootComponent$$$(), cc.xy(3, 5, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordNumberLabel2.setText("2.");
        statsRootPane.add(wordNumberLabel2, cc.xy(7, 3, CellConstraints.CENTER, CellConstraints.DEFAULT));
        statsRootPane.add(accuracyPane2.$$$getRootComponent$$$(), cc.xy(7, 5, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordNumberLabel3.setText("3.");
        statsRootPane.add(wordNumberLabel3, cc.xy(11, 3, CellConstraints.CENTER, CellConstraints.DEFAULT));
        statsRootPane.add(accuracyPane3.$$$getRootComponent$$$(), cc.xy(11, 5, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordLabel1.setText("");
        statsRootPane.add(wordLabel1, cc.xy(3, 7, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordNumberLabel4.setText("4.");
        statsRootPane.add(wordNumberLabel4, cc.xy(15, 3));
        statsRootPane.add(accuracyPane4.$$$getRootComponent$$$(), cc.xy(15, 5));
        wordNumberLabel5.setText("5.");
        statsRootPane.add(wordNumberLabel5, cc.xy(19, 3, CellConstraints.CENTER, CellConstraints.DEFAULT));
        statsRootPane.add(accuracyPane5.$$$getRootComponent$$$(), cc.xy(19, 5, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordNumberLabel6.setText("6.");
        statsRootPane.add(wordNumberLabel6, cc.xy(23, 3, CellConstraints.CENTER, CellConstraints.DEFAULT));
        statsRootPane.add(accuracyPane6.$$$getRootComponent$$$(), cc.xy(23, 5, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordNumberLabel7.setText("7.");
        statsRootPane.add(wordNumberLabel7, cc.xy(27, 3, CellConstraints.CENTER, CellConstraints.DEFAULT));
        statsRootPane.add(accuracyPane7.$$$getRootComponent$$$(), cc.xy(27, 5, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordNumberLabel8.setText("8.");
        statsRootPane.add(wordNumberLabel8, cc.xy(31, 3, CellConstraints.CENTER, CellConstraints.DEFAULT));
        statsRootPane.add(accuracyPane8.$$$getRootComponent$$$(), cc.xy(31, 5, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordNumberLabel9.setText("9.");
        statsRootPane.add(wordNumberLabel9, cc.xy(35, 3, CellConstraints.CENTER, CellConstraints.DEFAULT));
        statsRootPane.add(accuracyPane9.$$$getRootComponent$$$(), cc.xy(35, 5, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordNumberLabel10.setText("10.");
        statsRootPane.add(wordNumberLabel10, cc.xy(39, 3, CellConstraints.CENTER, CellConstraints.DEFAULT));
        statsRootPane.add(accuracyPane10.$$$getRootComponent$$$(), cc.xy(39, 5, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordLabel2.setText("");
        statsRootPane.add(wordLabel2, cc.xy(7, 7, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordLabel3.setText("");
        statsRootPane.add(wordLabel3, cc.xy(11, 7, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordLabel4.setText("");
        statsRootPane.add(wordLabel4, cc.xy(15, 7, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordLabel5.setText("");
        statsRootPane.add(wordLabel5, cc.xy(19, 7, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordLabel6.setText("");
        statsRootPane.add(wordLabel6, cc.xy(23, 7, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordLabel7.setText("");
        statsRootPane.add(wordLabel7, cc.xy(27, 7, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordLabel8.setText("");
        statsRootPane.add(wordLabel8, cc.xy(31, 7, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordLabel9.setText("");
        statsRootPane.add(wordLabel9, cc.xy(35, 7, CellConstraints.CENTER, CellConstraints.DEFAULT));
        wordLabel10.setText("");
        statsRootPane.add(wordLabel10, cc.xy(39, 7, CellConstraints.CENTER, CellConstraints.DEFAULT));
        sep1.setOrientation(1);
        statsRootPane.add(sep1, cc.xywh(5, 3, 1, 5, CellConstraints.FILL, CellConstraints.FILL));
        sep2.setOrientation(1);
        statsRootPane.add(sep2, cc.xywh(9, 3, 1, 5, CellConstraints.FILL, CellConstraints.FILL));
        sep3.setOrientation(1);
        statsRootPane.add(sep3, cc.xywh(13, 3, 1, 5, CellConstraints.FILL, CellConstraints.FILL));
        sep4.setOrientation(1);
        statsRootPane.add(sep4, cc.xywh(17, 3, 1, 5, CellConstraints.FILL, CellConstraints.FILL));
        sep5.setOrientation(1);
        statsRootPane.add(sep5, cc.xywh(21, 3, 1, 5, CellConstraints.FILL, CellConstraints.FILL));
        sep6.setOrientation(1);
        statsRootPane.add(sep6, cc.xywh(25, 3, 1, 5, CellConstraints.FILL, CellConstraints.FILL));
        sep7.setOrientation(1);
        statsRootPane.add(sep7, cc.xywh(29, 3, 1, 5, CellConstraints.FILL, CellConstraints.FILL));
        sep8.setOrientation(1);
        statsRootPane.add(sep8, cc.xywh(33, 3, 1, 5, CellConstraints.FILL, CellConstraints.FILL));
        sep9.setOrientation(1);
        statsRootPane.add(sep9, cc.xywh(37, 3, 1, 5, CellConstraints.FILL, CellConstraints.FILL));
        accuracyPercentileLabel.setText("Label");
        statsRootPane.add(accuracyPercentileLabel, cc.xyw(3, 9, 37, CellConstraints.CENTER, CellConstraints.DEFAULT));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return statsRootPane;
    }
}
