package gui;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import listeners.SwitchToMainMenuActionListener;
import processHandler.workers.DictionaryWorker;
import util.Config;
import util.VUtil;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * Settings menu of Voxspell
 */
public class ConfigMenu {
    protected JPanel outerConfigPanel;
    private JComboBox voiceComboBox;
    private JLabel voiceLabel;
    private JPanel innerConfigPanel;
    private JButton saveConfigButton;
    private JButton mainMenuButton;
    private JButton chooseFileButton;
    private JLabel fileLabel;
    private JButton cacheButton;
    private JLabel progressLabel;
    private JLabel titleLabel;
    private JFileChooser fileChooser;
    private File pendingFile;
    private DictionaryWorker dictionaryWorker;
    private final ActionListener cancelCacheActionListener;
    private ActionListener cacheActionListener;

    public ConfigMenu() {
        $$$setupUI$$$();
        mainMenuButton.addActionListener(new SwitchToMainMenuActionListener());
        saveConfigButton.addActionListener(new SwitchToMainMenuActionListener() {
            @Override
            public void extraAction() {
                if (pendingFile != null) {
                    // If the word list is changed and valid
                    Config.getInstance().setCurrentWordFile(pendingFile);
                    Config.getInstance().setCurrentWordList(pendingFile.getName());
                    VUtil.getInstance().changeWordList(pendingFile);
                }
                // Change the voice
                for (Map.Entry<String, String> e : Config.getInstance().voiceMap.entrySet()) {
                    if (e.getValue().equals(voiceComboBox.getModel().getSelectedItem())) {
                        Config.getInstance().setVoice(e.getKey());
                        break;
                    }
                }

            }

        });

        chooseFileButton.addActionListener(actionEvent -> {
            if ((fileChooser.showOpenDialog(outerConfigPanel)) == JFileChooser.APPROVE_OPTION) {
                // Opens the file selector
                File file = fileChooser.getSelectedFile();
                if (VUtil.checkWordList(file)) {
                    // Invalid file
                    JOptionPane.showMessageDialog(outerConfigPanel, file.getName() + " is not a valid wordlist file.");
                } else {
                    // Valid file
                    VUtil.removeAllListenersFrom(cacheButton);
                    cacheButton.addActionListener(cacheActionListener);
                    pendingFile = file;
                    fileLabel.setText(file.getName());
                }
            }

        });

        cancelCacheActionListener = actionEvent -> {
            if (dictionaryWorker != null) {
                // Stop the current worker
                dictionaryWorker.cancel(true);
                VUtil.removeAllListenersFrom(cacheButton);
                cacheButton.addActionListener(cacheActionListener);
                cacheButton.setText("Cache");
            }
        };

        cacheActionListener = actionEvent -> {
            List<String> list = VUtil.getInstance().getWordList().getAllWords();

            dictionaryWorker = new DictionaryWorker(list);

            // Listen to change in the worker and update accordingly
            dictionaryWorker.addPropertyChangeListener(propertyChangeEvent -> {
                if (propertyChangeEvent.getPropertyName().equals("Progress")) {
                    progressLabel.setText(String.format("%.2f%%", (Double) propertyChangeEvent.getNewValue() * 100));
                } else if (propertyChangeEvent.getNewValue().toString().equals("DONE") && !dictionaryWorker.isCancelled()) {
                    progressLabel.setText("100%");
                    cacheButton.setText("Done");
                    VUtil.removeAllListenersFrom(cacheButton);
                    cacheButton.setEnabled(false);
                } else if (dictionaryWorker.isCancelled()) {
                    progressLabel.setText("100%");
                    cacheButton.setText("Cache");
                    VUtil.removeAllListenersFrom(cacheButton);
                    cacheButton.setEnabled(true);
                }
            });
            dictionaryWorker.execute();
            cacheButton.setText("Cancel");
            VUtil.removeAllListenersFrom(cacheButton);
            cacheButton.addActionListener(cancelCacheActionListener);
        };

        cacheButton.addActionListener(cacheActionListener);
    }

    /**
     * Needed by UIDesigner
     */
    private void createUIComponents() {
        voiceComboBox = new JComboBox(Config.VOICES_LIST);
        voiceComboBox.getModel().setSelectedItem(Config.getInstance().voiceMap.get(Config.getInstance().getVoice()));

        fileChooser = new JFileChooser();
        // Make only .txt files acceptable
        fileChooser.addChoosableFileFilter(new FileFilter() {
            @Override
            public boolean accept(File file) {
                String fname = file.getName();
                int index = fname.lastIndexOf('.');
                if (index > 0) {
                    if (fname.substring(index + 1).equals("txt")) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public String getDescription() {
                return ".txt";
            }
        });
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(true);

        cacheButton = new JButton();
        progressLabel = new JLabel();

    }

    /**
     * Update the UI of the config menu, should be called each time before displaying this menu
     */
    public void updateUI() {
        voiceComboBox.setModel(new DefaultComboBoxModel(Config.getInstance().voiceMap.values().toArray()));
        voiceComboBox.getModel().setSelectedItem(Config.getInstance().voiceMap.get(Config.getInstance().getVoice()));
        outerConfigPanel.repaint();
        fileLabel.setText(Config.getInstance().getCurrentWordList());
        progressLabel.setText("");
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        outerConfigPanel = new JPanel();
        outerConfigPanel.setLayout(new FormLayout("fill:max(d;4px):grow(0.5),left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:145px:noGrow,left:4dlu:noGrow,fill:145px:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:max(d;4px):grow(0.5)", "center:9px:noGrow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:max(d;4px):grow,top:4dlu:noGrow,center:max(d;4px):grow,top:4dlu:noGrow,center:max(d;4px):noGrow,top:4dlu:noGrow,center:8px:noGrow"));
        innerConfigPanel = new JPanel();
        innerConfigPanel.setLayout(new FormLayout("fill:max(d;4px):grow,left:4dlu:noGrow,fill:d:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow(2.3),left:4dlu:noGrow,fill:max(d;4px):noGrow,left:4dlu:noGrow,fill:max(d;4px):grow", "center:d:grow"));
        CellConstraints cc = new CellConstraints();
        outerConfigPanel.add(innerConfigPanel, cc.xywh(1, 7, 11, 2));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new FormLayout("fill:d:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow,left:4dlu:noGrow,fill:max(d;4px):noGrow", "center:d:grow,top:4dlu:noGrow,center:max(d;4px):grow,top:4dlu:noGrow,center:max(d;4px):grow"));
        innerConfigPanel.add(panel1, cc.xyw(3, 1, 5));
        panel1.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), null, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, null, new Color(-16777216)));
        voiceLabel = new JLabel();
        voiceLabel.setIcon(new ImageIcon(getClass().getResource("/com/alee/laf/colorchooser/icons/color_chooser.png")));
        voiceLabel.setText("Voice");
        panel1.add(voiceLabel, cc.xy(1, 1));
        panel1.add(voiceComboBox, cc.xyw(3, 1, 3));
        final JLabel label1 = new JLabel();
        label1.setIcon(new ImageIcon(getClass().getResource("/com/alee/managers/style/icons/component/list.png")));
        label1.setText("Word List");
        panel1.add(label1, cc.xy(1, 3));
        fileLabel = new JLabel();
        fileLabel.setText("Label");
        panel1.add(fileLabel, cc.xy(3, 3, CellConstraints.RIGHT, CellConstraints.DEFAULT));
        chooseFileButton = new JButton();
        chooseFileButton.setText("Choose File");
        panel1.add(chooseFileButton, cc.xy(5, 3));
        final JLabel label2 = new JLabel();
        label2.setText("<html>Obtain Definitions</html>");
        panel1.add(label2, cc.xy(1, 5));
        cacheButton.setText("Cache");
        panel1.add(cacheButton, cc.xy(5, 5));
        progressLabel.setText("");
        panel1.add(progressLabel, cc.xy(3, 5, CellConstraints.RIGHT, CellConstraints.DEFAULT));
        saveConfigButton = new JButton();
        saveConfigButton.setIcon(new ImageIcon(getClass().getResource("/com/alee/extended/ninepatch/icons/save.png")));
        saveConfigButton.setText("<html>Save Settings</html>");
        outerConfigPanel.add(saveConfigButton, cc.xy(5, 9));
        mainMenuButton = new JButton();
        mainMenuButton.setIcon(new ImageIcon(getClass().getResource("/com/alee/managers/style/icons/component/menu.png")));
        mainMenuButton.setText("<html>Main Menu</html>");
        outerConfigPanel.add(mainMenuButton, cc.xy(7, 9));
        titleLabel = new JLabel();
        titleLabel.setIcon(new ImageIcon(getClass().getResource("/resources/settings.png")));
        titleLabel.setText("");
        outerConfigPanel.add(titleLabel, cc.xyw(3, 3, 7, CellConstraints.CENTER, CellConstraints.DEFAULT));
        voiceLabel.setLabelFor(voiceComboBox);
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return outerConfigPanel;
    }
}
