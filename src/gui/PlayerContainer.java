package gui;

import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.FormLayout;
import listeners.SwitchToMainMenuActionListener;
import listeners.SwitchToQuizModeActionListener;
import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerEventAdapter;
import util.Config;
import util.VUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;

/**
 * Player Container, solves some problem related to VLCJ
 */
public class PlayerContainer {
    private JPanel tp;
    private JButton nextLevelButton;
    private PlayerPanel playerPanel;
    private JButton pauseButton;
    private JButton volumeButton;
    private JSlider volumeSlider;
    private JButton mainMenuButton;
    private JButton stopButton;
    private JCheckBox reduceColorCheckBox;
    private JLabel timeLabel;

    private boolean isPaused = false;
    private boolean isStopped = true;
    private boolean isMuted = false;

    public PlayerContainer() {
        $$$setupUI$$$();

        playerPanel.preparePlayerWithDefaultMedia();
        pauseButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (isStopped) {
                    playerPanel.resetPlayer();
                    playerPanel.startPlayer();
                    pauseButton.setIcon(new ImageIcon(getClass().getResource("/resources/icons/Media-Controls-Pause-icon.png")));
                    isStopped = false;
                    return;
                }
                if (!isPaused) {
                    playerPanel.pausePlayer();
                    isPaused = true;
                    pauseButton.setIcon(new ImageIcon(getClass().getResource("/resources/icons/Media-Controls-Play-icon.png")));
                } else {
                    playerPanel.resumePlayer();
                    isPaused = false;
                    pauseButton.setIcon(new ImageIcon(getClass().getResource("/resources/icons/Media-Controls-Pause-icon.png")));
                }
            }
        });

        volumeButton.addActionListener(actionEvent -> {
            if (isMuted) {
                playerPanel.unmutePlayer();
                volumeButton.setIcon(Config.getInstance().VOLUME_UP_ICON);
                isMuted = false;
            } else {
                //!isMuted
                playerPanel.mutePlayer();
                volumeButton.setIcon(Config.getInstance().MUTE_ICON);
                isMuted = true;
            }
        });

        volumeSlider.addChangeListener(changeEvent -> playerPanel.setPlayerVolume(volumeSlider.getValue()));

        mainMenuButton.addActionListener(new SwitchToMainMenuActionListener() {
            @Override
            public void extraAction() {
                playerPanel.resetPlayer();
            }
        });

        stopButton.addActionListener(e -> {
            playerPanel.stopPlayer();
            pauseButton.setIcon(Config.getInstance().PLAY_ICON);
            isStopped = true;
            isPaused = false;

        });

        reduceColorCheckBox.addItemListener(itemEvent -> {
            if (itemEvent.getStateChange() == ItemEvent.SELECTED) {
                playerPanel.replaceWithAltMedia();
            } else if (itemEvent.getStateChange() == ItemEvent.DESELECTED) {
                playerPanel.replaceWithOriginalMedia();
            }

        });

    }


    /**
     * Update the PlayerContainer's UI with the next level
     *
     * @param nextLevel the next level to be tested, 0 if there is none
     */
    public void updateUI(int nextLevel) {
        volumeSlider.setValue(Config.INITIAL_VOLUME);
        pauseButton.setIcon(Config.getInstance().PLAY_ICON);
        volumeButton.setIcon(Config.getInstance().VOLUME_UP_ICON);
        isMuted = false;
        isPaused = false;
        isStopped = true;
        playerPanel.resetPlayer();

        if (nextLevel == 0) {
            nextLevelButton.setEnabled(false);
        } else {
            VUtil.removeAllListenersFrom(nextLevelButton);
            nextLevelButton.addActionListener(new SwitchToQuizModeActionListener() {
                @Override
                public void extraAction() {
                    UIController.getInstance().updateQuizWithLevel(nextLevel, Config.mode.NEW_QUIZ);
                    playerPanel.stopPlayer();
                }
            });
            nextLevelButton.setEnabled(true);
        }

        playerPanel.getPlayer().addMediaPlayerEventListener(new MediaPlayerEventAdapter() {
            @Override
            public void stopped(MediaPlayer mediaPlayer) {
                pauseButton.setIcon(Config.getInstance().PLAY_ICON);
                isPaused = false;
                isStopped = true;
            }

            @Override
            public void finished(MediaPlayer mediaPlayer) {
                pauseButton.setIcon(Config.getInstance().PLAY_ICON);
                isPaused = false;
                isStopped = true;
            }

            @Override
            public void playing(MediaPlayer mediaPlayer) {
                pauseButton.setIcon(Config.getInstance().PAUSE_ICON);
                isPaused = false;
                isStopped = false;

            }

            @Override
            public void paused(MediaPlayer mediaPlayer) {
                pauseButton.setIcon(Config.getInstance().PLAY_ICON);
                isPaused = true;
                isStopped = false;
            }


        });

        UIController.getInstance().updatePlayerSize();
        playerPanel.repaint();

    }

    /**
     * Needed by IDEA UI designer
     */
    private void createUIComponents() {
        timeLabel = new JLabel();
        // Parse the time into human readable format
        Timer timer = new Timer(500, actionEvent -> {
            double time = playerPanel.getTime();
            if (time == -1) {
                timeLabel.setText("00:00:00");
            } else {
                int sec = (int) time / 1000;
                int min = sec / 60;
                int hour = min / 60;
                sec = sec % 60;
                min = min % 60;
                timeLabel.setText(String.format("%02d:%02d:%02d", hour, min, sec));
            }
        });
        timer.start();
        playerPanel = new PlayerPanel();
    }


    /**
     * Update the player size
     *
     * @param dimension the dimension to update to
     * @see PlayerPanel
     */
    public void updatePlayerSize(Dimension dimension) {
        playerPanel.updatePlayerSize(new Dimension(dimension.width, dimension.height - 180));
        playerPanel.repaint();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        createUIComponents();
        tp = new JPanel();
        tp.setLayout(new FormLayout("fill:max(d;4px):grow,left:4dlu:noGrow,fill:150px:grow,left:4dlu:noGrow,fill:max(d;4px):noGrow,left:4dlu:noGrow,fill:max(d;4px):noGrow,left:4dlu:noGrow,fill:max(d;4px):noGrow,center:d:noGrow,left:4dlu:noGrow,center:max(d;4px):noGrow,left:4dlu:noGrow,fill:d:noGrow,left:4dlu:noGrow,fill:120px:noGrow,left:4dlu:noGrow,fill:120px:noGrow,left:4dlu:noGrow,left:4dlu:noGrow,fill:max(d;4px):grow", "fill:max(d;4px):noGrow,top:4dlu:noGrow,fill:p:grow,top:4dlu:noGrow,center:50px:noGrow,top:4dlu:noGrow,center:8px:noGrow"));
        CellConstraints cc = new CellConstraints();
        tp.add(playerPanel.$$$getRootComponent$$$(), cc.xyw(1, 3, 21));
        nextLevelButton = new JButton();
        nextLevelButton.setIcon(new ImageIcon(getClass().getResource("/com/alee/extended/filechooser/icons/forward.png")));
        nextLevelButton.setText("<html>Next Level</html>");
        tp.add(nextLevelButton, cc.xy(16, 5));
        volumeButton = new JButton();
        volumeButton.setIcon(new ImageIcon(getClass().getResource("/resources/icons/Media-Controls-Volume-Up-icon.png")));
        volumeButton.setText("");
        tp.add(volumeButton, cc.xy(12, 5, CellConstraints.RIGHT, CellConstraints.DEFAULT));
        volumeSlider = new JSlider();
        volumeSlider.setMajorTickSpacing(10);
        tp.add(volumeSlider, cc.xy(14, 5, CellConstraints.FILL, CellConstraints.DEFAULT));
        mainMenuButton = new JButton();
        mainMenuButton.setIcon(new ImageIcon(getClass().getResource("/com/alee/managers/style/icons/component/menu.png")));
        mainMenuButton.setText("<html>Main Menu</html>");
        tp.add(mainMenuButton, cc.xy(18, 5));
        stopButton = new JButton();
        stopButton.setIcon(new ImageIcon(getClass().getResource("/resources/icons/Media-Controls-Stop-icon.png")));
        stopButton.setText("");
        tp.add(stopButton, cc.xy(9, 5));
        pauseButton = new JButton();
        pauseButton.setIcon(new ImageIcon(getClass().getResource("/resources/icons/Media-Controls-Play-icon.png")));
        pauseButton.setText("");
        tp.add(pauseButton, cc.xy(7, 5));
        reduceColorCheckBox = new JCheckBox();
        reduceColorCheckBox.setText("Reduce Colour");
        tp.add(reduceColorCheckBox, cc.xy(3, 5));
        timeLabel.setText("Label");
        tp.add(timeLabel, cc.xy(5, 5));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return tp;
    }
}
