package annotations;

/**
 * Unsafe calls might cause problems
 */
public @interface UnsafeCall {
    String value() default "Not a safe call.";
}
