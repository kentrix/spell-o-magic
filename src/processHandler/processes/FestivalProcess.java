package processHandler.processes;


import util.Config;

/**
 * Spawns a festival process, text-to-speech synthesis with a voice set be Config
 * @see Config
 * @see BashProcess
 */
public class FestivalProcess extends BashProcess {

    public FestivalProcess(String vocal) {
        super("echo -e \\(voice_" + Config.getInstance().getVoice() + "\\)\\(SayText\\ \\\"" + vocal + "\\\"\\) | festival --pipe\n");
    }
}
