package processHandler.processes;

import util.Config;
import util.VUtil;

/**
 * Break down the word to be pronounced
 */
public class FestivalBreakDown extends BashProcess {
    public FestivalBreakDown(String vocal) {
        super("__word=" + VUtil.stripPunctuation(vocal) + " ;" +
                "for (( i=0 ; i < ${#__word} ; i++ )); do echo -e \\(voice_" + Config.getInstance().getVoice() +
                "\\)\\(SayText\\ \\\" ${__word:$i:1} \\\"\\) | festival --pipe ; sleep 0.15 ; done " );
    }
}
