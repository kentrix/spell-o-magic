package processHandler.processes;


/**
 * A helper class to get all the voices in Festival
 */
public class FestivalGetVoicesListProcess extends BashProcess {
    public FestivalGetVoicesListProcess() {
        super("echo '(voice.list)' | festival -i | tr -d \"()\" | sed '$d' | grep -A 100 'festival>' | sed -e 's/festival>//' -e 's/ //' | tr ' ' '\\n' | awk '{print $1}' | tr '\\n' ' '");
    }
}
