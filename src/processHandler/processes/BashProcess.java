package processHandler.processes;

import java.io.IOException;

/**
 * A generic builder for Bash process
 * The bash binary need be to found in /bin/
 */
public class BashProcess {

    private final ProcessBuilder processBuilder;
    private Process process;

    BashProcess(String args) {
        processBuilder = new ProcessBuilder("/bin/bash", "-c", args);
    }

    /**
     * Start the process
     */
    public void start() {
        try {
            process = processBuilder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Destroy the built process
     */
    public void destroy() {
        process.destroy();
    }

    /**
     * Get the built process
     *
     * @return
     */
    public ProcessBuilder getBuiltProcess() {
        return processBuilder;
    }

    /**
     * Start the process built and return it
     * @return the built process
     */
    public Process startAndGetProcess() {
        this.start();
        return process;
    }

    /**
     * Blocking method to wait for the process to finish
     */
    public void waitFor() {
        try {
            process.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
