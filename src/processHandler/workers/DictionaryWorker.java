package processHandler.workers;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import util.Config;
import util.VUtil;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Obtain definitions over the internet
 */
public class DictionaryWorker extends SwingWorker<Map<String, Map<String, String>>, Double> {

    private final List<String> quizStrings;

    public DictionaryWorker(List<String> strings) {
        this.quizStrings = strings;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Map<String, Map<String, String>> doInBackground() throws Exception {
        Map<String, Map<String, String>> infoMap = new HashMap<>();
        final int SIZE = quizStrings.size();
        int count = 0;

        for(String s : quizStrings) {
            s = s.trim();

            if(VUtil.getInstance().getDictionaryMap().containsKey(s.toLowerCase())) {
                count++;
                continue;
            }

            if(isCancelled()) {
                return infoMap;
            }

            String url = String.format(Config.EXP_SENT_API_URL, URLEncoder.encode(s, "UTF-8"));
            URLConnection connection = new URL(url).openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0");
            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String json = reader.readLine();
            JsonParser parser = new JsonParser();

            JsonObject rootObj = parser.parse(json).getAsJsonObject();
            JsonArray results = rootObj.getAsJsonArray("results");

            for(JsonElement e: results) {
                String tmp = e.getAsJsonObject().get("headword").getAsString();
                tmp = tmp.replace("\"", "");

                if(tmp.equalsIgnoreCase(s)) {

                    infoMap.put(s.toLowerCase(), new HashMap<>());

                    String pos;
                    try {
                        pos = e.getAsJsonObject().get("part_of_speech").getAsString();
                        infoMap.get(s.toLowerCase()).put("part_of_speech", pos);
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    String def;
                    try {
                        def = e.getAsJsonObject().getAsJsonArray("senses").get(0).getAsJsonObject().getAsJsonArray("definition")
                                .get(0).getAsString();
                        infoMap.get(s.toLowerCase()).put("definition", def);
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    String sent;
                    try {
                        sent = e.getAsJsonObject().getAsJsonArray("senses").get(0).getAsJsonObject().getAsJsonArray("examples")
                                .get(0).getAsJsonObject().get("text").getAsString();
                        infoMap.get(s.toLowerCase()).put("example", sent.replace("\'", "").replace("‘", ""));
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    break;
                }

            }
            publish(((double) ++count / SIZE));
        }
        return infoMap;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void process(List<Double> chunk) {
        firePropertyChange("Progress", null, chunk.get(chunk.size()-1));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void done() {
        try {
            Map<String, Map<String, String>> infoMap;
            infoMap = get();
            VUtil.getInstance().jointDictionaryMap(infoMap);
            System.out.println("Cache finished.");
            System.out.println(VUtil.getInstance().getDictionaryMap());
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
}
