package processHandler.workers;

import com.sun.istack.internal.Nullable;
import gui.FestivalButtonsToggleable;
import processHandler.processes.BashProcess;
import processHandler.processes.FestivalBreakDown;
import util.VUtil;

import javax.swing.*;

/**
 * Spawn a Festival process which reads words out letter-by-letter
 */
public class FestivalBreakDownWorker extends SwingWorker<Void, Void> {

    private BashProcess process;
    private FestivalButtonsToggleable festivalButtonsToggleable;

    public FestivalBreakDownWorker(String vocal,@Nullable FestivalButtonsToggleable festivalButtonsToggleable) {
        if(vocal != null) {
            process = new FestivalBreakDown(vocal);
        }
        if(festivalButtonsToggleable != null) {
            this.festivalButtonsToggleable = festivalButtonsToggleable;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Void doInBackground() throws Exception {
        if(!VUtil.getInstance().getLockObject().tryLock()) {
            return null;
        }
        if(festivalButtonsToggleable != null) {
            festivalButtonsToggleable.disableAllFestivalButtons();
        }
        if(process != null) {
            process.start();
            process.waitFor();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void done() {
        if(festivalButtonsToggleable != null) {
            festivalButtonsToggleable.enableAllFestivalButtons();
        }
        VUtil.getInstance().getLockObject().unlock();
    }
}
