package processHandler.workers;

import javax.swing.*;
import java.util.concurrent.ExecutionException;

/**
 * A wav worker together with festival for sound rewards
 */
public class WavWorkerWithFestival extends WavWorker {

    private final SwingWorker worker;

    public WavWorkerWithFestival(String file, SwingWorker festivalWoker) {
        super(file);
        this.worker = festivalWoker;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void done() {
        try {
            get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        worker.execute();

    }
}
