package processHandler.workers;

import uk.co.caprica.vlcj.player.MediaPlayer;
import uk.co.caprica.vlcj.player.MediaPlayerFactory;

import javax.swing.*;
import java.io.File;

/**
 * Play the sound for Voxspell
 */
public class WavWorker extends SwingWorker<Void, Void> {

    private final String path;

    public WavWorker(String path) {
        this.path = path;
    }

    @Override
    protected Void doInBackground() throws Exception {
        MediaPlayerFactory mediaPlayerFactory = new MediaPlayerFactory();
        MediaPlayer player = mediaPlayerFactory.newHeadlessMediaPlayer();
        player.playMedia(path);
        return null;
    }
}
