package processHandler.workers;

import javafx.embed.swing.JFXPanel;
import javafx.geometry.Side;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import util.VUtil;
import util.wordsManagement.ManagedWordList;

import javax.swing.*;
import java.util.*;
import java.util.concurrent.ExecutionException;

/**
 * Created by edward on 10/23/16.
 */
public class StatsChartWorker extends SwingWorker<Scene, Void> {

    private final JFXPanel jfxPanel;

    public StatsChartWorker(JFXPanel jfxPanel) {
        this.jfxPanel = jfxPanel;
    }
    @Override
    protected Scene doInBackground() throws Exception {

        ManagedWordList managedWordList = VUtil.getInstance().getManagedWordList();
        Set<String> stringSet;
        stringSet = new HashSet<>();
        stringSet.addAll(managedWordList.getFailed().keySet());
        stringSet.addAll(managedWordList.getFaulted().keySet());
        stringSet.addAll(managedWordList.getMastered().keySet());
        ArrayList<String> stringList = new ArrayList<>(stringSet);
        Collections.sort(stringList);

        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final BarChart<String, Number> bc =
                new BarChart<>(xAxis, yAxis);
        bc.setTitle("Stats Summary");
        xAxis.setLabel("Word");
        yAxis.setLabel("Number");

        XYChart.Series series1 = new XYChart.Series();
        series1.setName("Mastered");
        for (Map.Entry entry : VUtil.getInstance().getManagedWordList().getMastered().entrySet()) {
            series1.getData().add(new XYChart.Data(entry.getKey(), entry.getValue()));
        }

        XYChart.Series series2 = new XYChart.Series();
        series2.setName("Faulted");
        for (Map.Entry entry : VUtil.getInstance().getManagedWordList().getFaulted().entrySet()) {
            series2.getData().add(new XYChart.Data(entry.getKey(), entry.getValue()));
        }

        XYChart.Series series3 = new XYChart.Series();
        series3.setName("Failed");
        for (Map.Entry entry : VUtil.getInstance().getManagedWordList().getFailed().entrySet()) {
            series3.getData().add(new XYChart.Data(entry.getKey(), entry.getValue()));
        }

        XYChart.Series series4 = new XYChart.Series();
        series4.setName("Learned");
        for (Map.Entry entry : VUtil.getInstance().getManagedWordList().getLearned().entrySet()) {
            series4.getData().add(new XYChart.Data(entry.getKey(), entry.getValue()));
        }

        Scene scene = new Scene(bc, stringList.size() * 200, 600);
        bc.getData().addAll(series1, series2, series3, series4);
        bc.setLegendSide(Side.LEFT);
        return scene;
    }

    @Override
    public void done() {
        try {
            jfxPanel.setScene(get());
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
