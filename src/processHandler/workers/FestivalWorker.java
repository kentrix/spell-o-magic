package processHandler.workers;

import com.sun.istack.internal.Nullable;
import gui.FestivalButtonsToggleable;
import processHandler.processes.BashProcess;
import processHandler.processes.FestivalProcess;
import util.Config;
import util.VUtil;

import javax.swing.*;

/**
 * A container class for a festival process
 * <p>
 *     Uses SwingWorker instead of running on the ED thread
 * </p>
 */
public class FestivalWorker extends SwingWorker<Boolean, Integer> {

    private final BashProcess bashProcess1;
    private FestivalWorker festivalWorker;
    private JButton button;
    private String setTo;
    private FestivalButtonsToggleable festivalButtonsToggleable;


    /**
     * Construct a FestivalWorker for TTS
     * @param v1 the first String to be voiced
     * @param v2 the second String to be voiced
     * @param button the button which this worker to update
     * @param setTo the text of the button to update
     * @param festivalButtonsToggleable A object the implements the FestivalButtonsToggleable interface
     */
    public FestivalWorker(@Nullable String v1,@Nullable String v2,@Nullable JButton button,@Nullable String setTo,
                          @Nullable FestivalButtonsToggleable festivalButtonsToggleable) {
        this.bashProcess1 = new FestivalProcess(v1);
        if(v2 != null) {
            this.festivalWorker = new FestivalWorker(v2, festivalButtonsToggleable);
        }
        if(button != null) {
            this.button = button;
        }
        if(setTo != null) {
            this.setTo = setTo;
        }
        if(festivalButtonsToggleable != null) {
            this.festivalButtonsToggleable = festivalButtonsToggleable;
        }

    }

    /**
     * Construct a FestivalWorker for TTS
     * @param v1 the first String to be voiced
     * @param festivalButtonsToggleable A object the implements the FestivalButtonsToggleable interface
     */
    public FestivalWorker(String v1, FestivalButtonsToggleable festivalButtonsToggleable) {
        this(v1, null, null, null, festivalButtonsToggleable);
    }

    /**
     * Construct a FestivalWorker for TTS
     * @param v1 the first String to be voiced
     * @param v2 the second String to be voiced
     * @param festivalButtonsToggleable A object the implements the FestivalButtonsToggleable interface
     */
    public FestivalWorker(String v1, String v2, FestivalButtonsToggleable festivalButtonsToggleable) {
        this(v1, v2, null, null, festivalButtonsToggleable);
    }

    /**
     * Construct a FestivalWorker for TTS
     * @param v1 the first String to be voiced
     * @param v2 the second String to be voiced
     * @param button the button which this worker to update
     * @param setTo the text of the button to update
     */
    public FestivalWorker(String v1, String v2, JButton button, String setTo) {
        this(v1, v2, button, setTo, null);
    }


    /**
     * Construct a FestivalWorker for TTS
     * The text of the button is updated by default behavior
     * @param v1 the first String to be voiced
     * @param v2 the second String to be voiced
     * @param button the button which this worker to update
     */
    private FestivalWorker(String v1, String v2, JButton button) {
        this(v1, v2, button, null);
    }

    /**
     * Construct a FestivalWorker for TTS
     * The text of the button is updated by default behavior
     * @param vocal the String to be voiced
     * @param button the button which this worker to update
     */
    public FestivalWorker(String vocal, JButton button) {
        this(vocal, null, button);
    }

    /**
     * Construct a FestivalWorker for TTS
     * The text of the button is updated by default behavior
     * @param v1 the first String to be voiced
     * @param v2 the second String to be voiced
     */
    public FestivalWorker(String v1, String v2) {
        this(v1, v2, null, null);
    }

    /**
     * Construct a FestivalWorker for TTS
     * The text of the button is updated by default behavior
     * @param vocal the String to be voiced
     */
    public FestivalWorker(String vocal) {
        this(vocal, null, null, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Boolean doInBackground() throws Exception {
        if(!VUtil.getInstance().getLockObject().tryLock()) {
            return false;
        }

        if(festivalButtonsToggleable != null) {
            festivalButtonsToggleable.disableAllFestivalButtons();
        }
        if(button != null) {
            button.setText("Pronouncing...");
            button.setEnabled(false);
        }
        bashProcess1.start();
        bashProcess1.waitFor();
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void done() {
        if(button != null && festivalWorker == null) {
            button.setText(setTo == null ? "Submit" : setTo);
            button.setEnabled(true);
        }
        if(festivalWorker!= null && festivalButtonsToggleable != null) {
            javax.swing.Timer timer = new Timer(Config.TTS_DELAY ,actionEvent ->  {
                festivalWorker.execute();
                if(button != null) {
                    button.setText(setTo == null ? "Submit" : setTo);
                    button.setEnabled(true);
                }
            });
            timer.setRepeats(false);
            timer.start();
        }
        else if(festivalButtonsToggleable != null) {
            festivalButtonsToggleable.enableAllFestivalButtons();
        }
        VUtil.getInstance().getLockObject().unlock();

    }
}
