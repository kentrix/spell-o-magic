package exceptions;

/**
 * This exception is thrown when there is incorrect operation is performed
 * on a display pane.
 */
public class InvalidPushOperationException extends Exception {
    public InvalidPushOperationException() {
        super("Invalid push operation.");
    }
}
