package exceptions;

/**
 * This is the exception thrown when the word list file is has an invalid format
 */
public class InvalidFileFormatException extends Exception{
    public InvalidFileFormatException() {
        super("Selected file has a invalid format.");
    }
}
