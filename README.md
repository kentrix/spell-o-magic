Spell-o-magic - A spelling aid
=========================
### Build instructions
with [Intellij IDEA](https://github.com/JetBrains/intellij-community)
Clone this repo -> build artifact

### Dependencies
OpenJDK/JRE or Orcale JDK/JRE 1.8 or above

[vlcj](https://github.com/caprica/vlcj)
for media playback

[weblaf](https://github.com/mgarin/weblaf)
for the look and feel

[jgoodies](https://mvnrepository.com/artifact/com.jgoodies)
for the layout manager

### Licence
MIT License

Copyright (c) 2016 Edward Huang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

